﻿// <copyright file="OrderedToyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using ToyShop.Data;

    /// <inheritdoc/>
    public class OrderedToyRepository : GenericRepository<OrderedToy>, IOrderedToyRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderedToyRepository"/> class.
        /// </summary>
        /// <param name="context"> Database reference. </param>
        public OrderedToyRepository(DbContext context)
            : base(context)
        {
        }

        /// <inheritdoc/>
        public override OrderedToy GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(toy => toy.ID == id);
        }

        /// <inheritdoc/>
        public void UpdatePrice(int id, int newPrice)
        {
            this.GetOne(id).Price = newPrice;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateQuantity(int id, int newQuantity)
        {
            this.GetOne(id).Quantity = newQuantity;
            this.Context.SaveChanges();
        }
    }
}
