﻿// <copyright file="IOrderRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using ToyShop.Data;

    /// <summary>
    /// CRUD and non-Crud functions for orders.
    /// </summary>
    public interface IOrderRepository : IRepository<Order>
    {
        /// <summary>
        /// Updates the ordered toy's admin with the given ID.
        /// </summary>
        /// <param name="id"> The ID of the order. </param>
        /// <param name="newAdmin"> The ID of new admin. </param>
        void UpdateOrderAdmin(int id, int newAdmin);

        /// <summary>
        /// Adds a toy to an order.
        /// </summary>
        /// <param name="id"> The ID of the order. </param>
        /// <param name="toy"> The toy to add. </param>
        /// <param name="qty"> They quantity of the toy. </param>
        void AddToyToOrder(int id, Toy toy, int qty);

        /// <summary>
        /// Removes a toy from an order.
        /// </summary>
        /// <param name="toyId"> The id of the removable toy. </param>
        void RemoveToyFromOrder(int toyId);
    }
}