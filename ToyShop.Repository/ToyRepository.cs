﻿// <copyright file="ToyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using ToyShop.Data;

    /// <summary>
    /// This class contains the CRUD and non-CRUD methods for toys.
    /// </summary>
    public class ToyRepository : GenericRepository<Toy>, IToyRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToyRepository"/> class.
        /// </summary>
        /// <param name="context"> This is the database that we inicialize. </param>
        public ToyRepository(DbContext context)
            : base(context)
        {
            /* ... */
        }

        /// <inheritdoc/>
        public override Toy GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(toy => toy.ID == id);
        }

        /// <inheritdoc/>
        public void UpdateManufacturer(int id, string newManufacturer)
        {
            this.GetOne(id).Manufacturer = newManufacturer;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateName(int id, string newName)
        {
            this.GetOne(id).Name = newName;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdatePrice(int id, int newPrice)
        {
            this.GetOne(id).Price = newPrice;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateQuantity(int id, int plusQuantity)
        {
            this.GetOne(id).Quantity += plusQuantity;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateToyCategory(int id, string category)
        {
            this.GetOne(id).Category = category;
            this.Context.SaveChanges();
        }
    }
}