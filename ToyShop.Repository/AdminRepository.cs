﻿// <copyright file="AdminRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace ToyShop.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using ToyShop.Data;

    /// <summary>
    /// This class contains the CRUD and non-CRUD methods for admins.
    /// </summary>
    public class AdminRepository : GenericRepository<Administrator>, IAdminRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdminRepository"/> class.
        /// </summary>
        /// <param name="context"> This is the database that we inicialize. </param>
        public AdminRepository(DbContext context)
            : base(context)
        {
            /* ... */
        }

        /// <inheritdoc/>
        public override Administrator GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(admin => admin.ID == id);
        }

        /// <inheritdoc/>
        public void UpdateEmail(int id, string newEmail)
        {
            this.GetOne(id).Email = newEmail;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateName(int id, string newName)
        {
            this.GetOne(id).Name = newName;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateOfficeID(int id, int newOfficeID)
        {
            this.GetOne(id).OfficeID = newOfficeID;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateRank(int id, string newRank)
        {
            this.GetOne(id).Rank = newRank;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateSalary(int id, int newSalary)
        {
            this.GetOne(id).Salary = newSalary;
            this.Context.SaveChanges();
        }
    }
}
