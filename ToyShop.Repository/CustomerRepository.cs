﻿// <copyright file="CustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using ToyShop.Data;

    /// <summary>
    /// This class contains the CRUD and non-CRUD methods for customers.
    /// </summary>
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="context"> This is the database that we inicialize. </param>
        public CustomerRepository(DbContext context)
            : base(context)
        {
        }

        /// <inheritdoc/>
        public override Customer GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(customer => customer.ID == id);
        }

        /// <inheritdoc/>
        public void UpdateCity(int id, string newCity)
        {
            this.GetOne(id).City = newCity;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateCountry(int id, string newCountry)
        {
            this.GetOne(id).Country = newCountry;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateEmail(int id, string newEmail)
        {
            this.GetOne(id).Email = newEmail;
        }

        /// <inheritdoc/>
        public void UpdateName(int id, string newName)
        {
            this.GetOne(id).Name = newName;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdatePassword(int id, string oldPassword, string newPassword)
        {
            this.GetOne(id).Password = newPassword;
            this.Context.SaveChanges();
        }
    }
}
