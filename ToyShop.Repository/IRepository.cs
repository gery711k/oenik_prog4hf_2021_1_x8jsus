﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using System.Linq;

    /// <summary>
    /// This is the repositry interface.
    /// </summary>
    /// <typeparam name="T"> Generic type. </typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Get an element using ID.
        /// </summary>
        /// <param name="id"> ID of the serched element. </param>
        /// <returns> The searched item. </returns>
        T GetOne(int id);

        /// <summary>
        /// Get all of the elemenets from a query.
        /// </summary>
        /// <returns> The query. </returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Add a new element to a table.
        /// </summary>
        /// <param name="newInstance"> The current element, that we will add to a table. </param>
        public void Add(T newInstance);

        /// <summary>
        /// Delete an element from a table.
        /// </summary>
        /// <param name="instance"> The current element, that we will delete from a table. </param>
        public void Delete(T instance);
    }
}
