﻿// <copyright file="ICustomerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using ToyShop.Data;

    /// <summary>
    /// CRUD and non-Crud functions for customers.
    /// </summary>
    public interface ICustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Updates the password of a customer.
        /// </summary>
        /// <param name="id"> The ID of a customer. </param>
        /// <param name="oldPassword"> The old password the customer. </param>
        /// <param name="newPassword"> The new password to set. </param>
        void UpdatePassword(int id, string oldPassword, string newPassword);

        /// <summary>
        /// Updates the name of a customer.
        /// </summary>
        /// <param name="id"> The ID of a customer. </param>
        /// <param name="newName">The new name of the customer. </param>
        void UpdateName(int id, string newName);

        /// <summary>
        /// Updates the country of a customer.
        /// </summary>
        /// <param name="id"> The ID of a customer. </param>
        /// <param name="newCountry">The new country of the customer. </param>
        void UpdateCountry(int id, string newCountry);

        /// <summary>
        /// Updates the city of a customer.
        /// </summary>
        /// <param name="id"> The ID of a customer. </param>
        /// <param name="newCity">The new city of the customer. </param>
        void UpdateCity(int id, string newCity);

        /// <summary>
        /// Updates the email of a customer.
        /// </summary>
        /// <param name="id"> The ID of a customer. </param>
        /// <param name="newEmail">The new email of the customer. </param>
        void UpdateEmail(int id, string newEmail);
    }
}
