﻿// <copyright file="GenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The parent of all the repository classes.
    /// </summary>
    /// <typeparam name="T"> The type of the repository's content. </typeparam>
    public abstract class GenericRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Gets a table's context from the constructor.
        /// </summary>
        private DbContext dbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{T}"/> class.
        /// Sets a given table's context.
        /// </summary>
        /// <param name="context"> The table's name that we want to access. </param>
        public GenericRepository(DbContext context)
        {
            this.dbContext = context;
        }

        /// <summary>
        /// Gets the the database's refence.
        /// </summary>
        protected DbContext Context
        {
            get { return this.dbContext; }
        }

        /// <inheritdoc/>
        public void Add(T newInstance)
        {
            this.dbContext.Set<T>().Add(newInstance);
            this.dbContext.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(T instance)
        {
            this.dbContext.Set<T>().Remove(instance);
            this.dbContext.SaveChanges();
        }

        /// <inheritdoc/>
        public IQueryable<T> GetAll()
        {
            return this.dbContext.Set<T>();
        }

        /// <inheritdoc/>
        public abstract T GetOne(int id);
    }
}
