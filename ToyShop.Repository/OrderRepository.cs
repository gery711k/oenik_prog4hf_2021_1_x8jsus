﻿// <copyright file="OrderRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using ToyShop.Data;

    /// <inheritdoc/>
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderRepository"/> class.
        /// </summary>
        /// <param name="context"> The order table's context. </param>
        public OrderRepository(DbContext context)
            : base(context)
        {
        }

        /// <inheritdoc/>
        public void AddToyToOrder(int id, Toy toy, int qty)
        {
            if (toy != null)
            {
                Order order = this.GetOne(id);

                OrderedToy otoy = new OrderedToy()
                {
                    Price = toy.Price,
                    OrderID = order.ID,
                    Quantity = qty,
                    ToyID = toy.ID,
                };

                this.Context.Set<OrderedToy>().Add(otoy);
            }

            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public override Order GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(order => order.ID == id);
        }

        /// <inheritdoc/>
        public void RemoveToyFromOrder(int toyId)
        {
            this.Context.Set<OrderedToy>().Remove(this.Context.Set<OrderedToy>().Find(toyId));
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateOrderAdmin(int id, int newAdmin)
        {
            this.GetOne(id).AdminID = newAdmin;
        }
    }
}