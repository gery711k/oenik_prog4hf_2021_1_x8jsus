﻿// <copyright file="IToyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using ToyShop.Data;

    /// <summary>
    /// CRUD and non-Crud functions for toys.
    /// </summary>
    public interface IToyRepository : IRepository<Toy>
    {
        /// <summary>
        /// Updates the price of a toy.
        /// </summary>
        /// <param name="id"> The ID of the toy. </param>
        /// <param name="newPrice"> The new price. </param>
        void UpdatePrice(int id, int newPrice);

        /// <summary>
        /// Updates the name of a toy.
        /// </summary>
        /// <param name="id"> The ID of the toy. </param>
        /// <param name="newName"> The new name. </param>
        void UpdateName(int id, string newName);

        /// <summary>
        /// Updates the manufacturer of a toy.
        /// </summary>
        /// <param name="id"> The toys id. </param>
        /// <param name="newManufacturer"> The new manufacturer. </param>
        void UpdateManufacturer(int id, string newManufacturer);

        /// <summary>
        /// Add's quantity for the toy.
        /// </summary>
        /// <param name="id"> The toys id. </param>
        /// <param name="plusQuantity"> The quantity to add. </param>
        void UpdateQuantity(int id, int plusQuantity);

        /// <summary>
        /// Updates the category of a toy.
        /// </summary>
        /// <param name="id">The toy's id.</param>
        /// <param name="category">The new category.</param>
        void UpdateToyCategory(int id, string category);
    }
}