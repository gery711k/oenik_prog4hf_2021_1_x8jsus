﻿// <copyright file="IOrderedToyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using ToyShop.Data;

    /// <summary>
    /// CRUD and non-Crud functions for orderedtoys.
    /// </summary>
    public interface IOrderedToyRepository : IRepository<OrderedToy>
    {
        /// <summary>
        /// Updates the price of a toy.
        /// </summary>
        /// <param name="id"> The ID of the toy. </param>
        /// <param name="newPrice"> The new price. </param>
        void UpdatePrice(int id, int newPrice);

        /// <summary>
        /// Updates the quatity of an ordered toy.
        /// </summary>
        /// <param name="id"> The toys id. </param>
        /// <param name="newQuantity"> The new quantity. </param>
        void UpdateQuantity(int id, int newQuantity);
    }
}
