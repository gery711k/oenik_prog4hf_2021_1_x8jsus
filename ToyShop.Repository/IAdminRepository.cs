﻿// <copyright file="IAdminRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Repository
{
    using ToyShop.Data;

    /// <summary>
    /// CRUD and non-Crud functions for admins.
    /// </summary>
    public interface IAdminRepository : IRepository<Administrator>
    {
        /// <summary>
        /// Updates an admins's name with the given ID.
        /// </summary>
        /// <param name="id"> The ID of an admin. </param>
        /// <param name="newName"> The new name of an admin. </param>
        void UpdateName(int id, string newName);

        /// <summary>
        /// Updates an admins's rank with the given ID.
        /// </summary>
        /// <param name="id"> The ID of an admin. </param>
        /// <param name="newRank"> The new rank of an admin. </param>
        void UpdateRank(int id, string newRank);

        /// <summary>
        /// Updates an admins's officeID with the given ID.
        /// </summary>
        /// <param name="id"> The ID of an admin. </param>
        /// <param name="newOfficeID">  The new office id of the admin. </param>
        void UpdateOfficeID(int id, int newOfficeID);

        /// <summary>
        /// Updates an admin's salary with the given ID.
        /// </summary>
        /// <param name="id"> The admins id. </param>
        /// <param name="newSalary"> The new salary. </param>
        void UpdateSalary(int id, int newSalary);

        /// <summary>
        /// Updates an admin's email with the given ID.
        /// </summary>
        /// <param name="id"> The admins id. </param>
        /// <param name="newEmail"> The new email. </param>
        void UpdateEmail(int id, string newEmail);
    }
}
