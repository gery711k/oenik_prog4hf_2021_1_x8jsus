﻿// <copyright file="IShopLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace ToyShop.Logic
{
    using System.Collections.Generic;
    using ToyShop.Data;

    /// <summary>
    /// This interface defines the basic shop related methods.
    /// </summary>
    public interface IShopLogic
    {
        /// <summary>
        /// Get an order with the given ID.
        /// </summary>
        /// <param name="id"> The ID of the searched order. </param>
        /// <returns> An Order type object. </returns>
        Order GetOrder(int id);

        /// <summary>
        /// Adds a new order to the database.
        /// </summary>
        /// <param name="newOrder"> The new Order that we will add to the database. </param>
        void AddOrder(Order newOrder);

        /// <summary>
        /// Adds a toy to an order.
        /// </summary>
        /// <param name="id"> Id of the order. </param>
        /// <param name="toy"> Toy to add. </param>
        /// <param name="quantity"> The quanntity of the toy. </param>
        /// <returns> True if method was successful. </returns>
        bool AddToyToOrder(int id, Toy toy, int quantity);

        /// <summary>
        /// Changes the quantity of an item in the order.
        /// </summary>
        /// <param name="toyId"> The updateable toy's id. </param>
        /// <param name="newQuantity"> The new quantity of the item. </param>
        /// <returns> True if method was successful. </returns>
        bool ChangeToyQuantityInOrder(int toyId, int newQuantity);

        /// <summary>
        /// Removes a toy from the order.
        /// </summary>
        /// <param name="orderId"> The id of the order. </param>
        /// <param name="toyId"> The removable toy's id. </param>
        /// <returns> True if method was successful. </returns>
        bool RemoveToyFromOrder(int orderId, int toyId);

        /// <summary>
        /// Deletes an order from the database.
        /// </summary>
        /// <param name="id"> The ID of the no longer needed order. </param>
        /// <returns> True if method was successful. </returns>
        bool DeleteOrder(int id);

        /// <summary>
        /// Updates an order's admin by ID.
        /// </summary>
        /// <param name="orderID"> The ID of the order. </param>
        /// <param name="newAdmin"> The ID of the orders new admin. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateOrdersAdmin(int orderID, Administrator newAdmin);

        /// <summary>
        /// Get all of the orders from the database.
        /// </summary>
        /// <returns> A list that contains all of the Order type items in the database. </returns>
        IList<Order> GetAllOrders();

        /// <summary>
        /// Adds a new Toy to the database.
        /// </summary>
        /// <param name="newToy"> The new Toy that we will add to the database. </param>
        void AddToy(Toy newToy);

        /// <summary>
        /// Deletes a Toy from the database.
        /// </summary>
        /// <param name="id"> The ID of the no longer needed Toy. </param>
        /// <returns> True if method was successful. </returns>
        bool DeleteToy(int id);

        /// <summary>
        /// Updates a Toy's price by ID.
        /// </summary>
        /// <param name="id"> The ID of the Toy we will update. </param>
        /// <param name="newPrice"> The new price of the Toy. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateToyPrice(int id, int newPrice);

        /// <summary>
        /// Updates a Toy's name by ID.
        /// </summary>
        /// <param name="id"> The id of the toy. </param>
        /// <param name="newName"> The new name of the Toy. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateToyName(int id, string newName);

        /// <summary>
        /// Updates a Toy's manufacturer by ID.
        /// </summary>
        /// <param name="id"> The id of the toy. </param>
        /// <param name="newManufacturer"> The new manufacturer of the Toy. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateToyManufacturer(int id, string newManufacturer);

        /// <summary>
        /// Updates a Toy's quantity by ID.
        /// </summary>
        /// <param name="id"> The id of the toy. </param>
        /// <param name="plusQuantity"> The quantity to add. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateToyQuantity(int id, int plusQuantity);

        /// <summary>
        /// Updates a toy's category by ID.
        /// </summary>
        /// <param name="id">The id of the toy.</param>
        /// <param name="newCategory">The new category.</param>
        /// <returns>True if method was successful.</returns>
        bool UpdateToyCategory(int id, string newCategory);

        /// <summary>
        /// Gets a toy with the given ID.
        /// </summary>
        /// <param name="id">The ID of the searched Toy. </param>
        /// <returns> A Toy type object. </returns>
        Toy GetToy(int id);

        /// <summary>
        /// Get all of the Toys from the database.
        /// </summary>
        /// <returns> A list that contains all of the Toy type objects in the database.</returns>
        IList<Toy> GetAllToys();

        /// <summary>
        /// Get an ordered toy with the given ID.
        /// </summary>
        /// <param name="id"> The ID of the searched toy. </param>
        /// <returns> An OrderedToy type object. </returns>
        OrderedToy GetOrderedToy(int id);

        /// <summary>
        /// Get all of the ordered toys from the database.
        /// </summary>
        /// <returns> Returns a list of OrderedToy type objects. </returns>
        IList<OrderedToy> GetAllOrderedToys();

        // NON-CRUD METHODS

        /// <summary>
        /// Get an order's content.
        /// </summary>
        /// <param name="id"> The ID of the order. </param>
        /// <returns> A list that contains the content of the order. </returns>
        IList<OrderedToy> GetOrderContent(int id);

        /// <summary>
        /// Get the average number of toys in the orders.
        /// </summary>
        /// <returns> A double average value. </returns>
        double GetAverageNumberOfToysInAllOrders();

        /// <summary>
        /// Gets the value of an order.
        /// </summary>
        /// <param name="id"> The ID of an order. </param>
        /// <returns> The integer sum value of the order content. </returns>
        int GetOrderValue(int id);

        /// <summary>
        /// Gets the value of all of the orders.
        /// </summary>
        /// <returns> An integer sum value of each order. </returns>
        Dictionary<string, int> GetAllOrdersValues();

        /// <summary>
        /// Gets a list of customer how has the given toy in its Order.
        /// </summary>
        /// <param name="id"> The ID of the toy. </param>
        /// <returns> A list of customer id's. </returns>
        IList<int> GetAllCustomerIdsWithGivenToyInOrder(int id);
    }
}
