﻿// <copyright file="IUserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Logic
{
    using System.Collections.Generic;
    using ToyShop.Data;

    /// <summary>
    /// This interface defines the basic user related methods.
    /// </summary>
    public interface IUserLogic
    {
        // Customer methods.

        /// <summary>
        /// Gets a customer with the given ID.
        /// </summary>
        /// <param name="id"> The ID of the searched customer. </param>
        /// <returns> A Customer type object. </returns>
        Customer GetCustomer(int id);

        /// <summary>
        /// Adds a new customer to the database.
        /// </summary>
        /// <param name="newCustomer"> The new Customer that we will add to the database. </param>
        void AddCustomer(Customer newCustomer);

        /// <summary>
        /// Deletes a customer from the database.
        /// </summary>
        /// <param name="id"> The ID of the no longer needed customer. </param>
        /// <returns> True if method was successful. </returns>
        bool DeleteCustomer(int id);

        /// <summary>
        /// Updates an customer's name.
        /// </summary>
        /// <param name="id"> The id of the customer. </param>
        /// <param name="newName"> The new name of the customer. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateCustomerName(int id, string newName);

        /// <summary>
        /// Updates a customer's password.
        /// </summary>
        /// <param name="id"> The id of the customer. </param>
        /// <param name="oldPassword"> The old password that we will exchange. </param>
        /// <param name="newPassword"> The new password that you will use. </param>
        /// <returns> Returns true, if the password change was successful, else returns false. </returns>
        bool UpdateCustomerPassword(int id, string oldPassword, string newPassword);

        /// <summary>
        /// Updates a customers city.
        /// </summary>
        /// <param name="id"> The id of the customer. </param>
        /// <param name="newCity">The new city. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateCustomerCity(int id, string newCity);

        /// <summary>
        /// Updates a customers country.
        /// </summary>
        /// <param name="id"> The customers id. </param>
        /// <param name="newCountry"> The customers new country. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateCustomerCountry(int id, string newCountry);

        /// <summary>
        /// Updates a customers email.
        /// </summary>
        /// <param name="id"> The customers id. </param>
        /// <param name="newEmail"> The customers new email. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateCustomerEmail(int id, string newEmail);

        /// <summary>
        /// Gets all of the customers from the database.
        /// </summary>
        /// <returns> A list that contains all of the Customer type items in the database. </returns>
        IList<Customer> GetAllCustomers();

        // Administrator methods.

        /// <summary>
        /// Get an admin with the given ID.
        /// </summary>
        /// <param name="id"> The ID of the admin. </param>
        /// <returns> An Admin type object. </returns>
        Administrator GetAdmin(int id);

        /// <summary>
        /// Adds a new admin to the database.
        /// </summary>
        /// <param name="newAdmin"> The new Admin that we will add to the database. </param>
        void AddAdmin(Administrator newAdmin);

        /// <summary>
        /// Deletes an admin from the database.
        /// </summary>
        /// <param name="id"> The ID of the no longer needed admin. </param>
        /// <returns> True if method was successful. </returns>
        bool DeleteAdmin(int id);

        /// <summary>
        /// Updates an admin's name.
        /// </summary>
        /// <param name="id"> The id of the admin. </param>
        /// <param name="newName"> The admin's new name. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateAdminName(int id, string newName);

        /// <summary>
        /// Updates an admin's rank.
        /// </summary>
        /// <param name="id"> The id of an admin. </param>
        /// <param name="newRank"> The admin's new rank. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateAdminRank(int id, string newRank);

        /// <summary>
        /// Updates an admin's office id.
        /// </summary>
        /// <param name="id"> The id of an admin. </param>
        /// <param name="newOfficeID"> The admin's new office id. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateAdminOffice(int id, int newOfficeID);

        /// <summary>
        /// Updates an admins email.
        /// </summary>
        /// <param name="id"> The admins id. </param>
        /// <param name="newEmail"> The new email. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateAdminEmail(int id, string newEmail);

        /// <summary>
        /// Updates an admins salary.
        /// </summary>
        /// <param name="id"> The admins id. </param>
        /// <param name="newSalary">The new salary. </param>
        /// <returns> True if method was successful. </returns>
        bool UpdateAdminSalary(int id, int newSalary);

        /// <summary>
        /// Get all of the admins from the database.
        /// </summary>
        /// <returns> A list that contains all of the admin type items in the database. </returns>
        IList<Administrator> GetAllAdmins();
    }
}
