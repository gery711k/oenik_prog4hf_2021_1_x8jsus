﻿// <copyright file="UserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using ToyShop.Data;
    using ToyShop.Repository;

    /// <summary>
    /// This class manages the users.
    /// </summary>
    public class UserLogic : IUserLogic
    {
        private IAdminRepository adminRepo;
        private ICustomerRepository customerRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserLogic"/> class.
        /// </summary>
        /// <param name="adminRepo">The repository of the admins table.</param>
        /// <param name="customerRepo">The repository of the customers table.</param>
        public UserLogic(IAdminRepository adminRepo, ICustomerRepository customerRepo)
        {
            this.adminRepo = adminRepo;
            this.customerRepo = customerRepo;
        }

        /// <inheritdoc/>
        public void AddAdmin(Administrator newAdmin)
        {
            this.adminRepo.Add(newAdmin);
        }

        /// <inheritdoc/>
        public void AddCustomer(Customer newCustomer)
        {
            this.customerRepo.Add(newCustomer);
        }

        /// <inheritdoc/>
        public bool DeleteAdmin(int id)
        {
            if (this.GetAdmin(id) != null)
            {
                this.adminRepo.Delete(this.adminRepo.GetOne(id));

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool DeleteCustomer(int id)
        {
            if (this.GetCustomer(id) != null)
            {
                this.customerRepo.Delete(this.customerRepo.GetOne(id));

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public Administrator GetAdmin(int id)
        {
            if (this.GetAllAdmins().Contains(this.adminRepo.GetOne(id)))
            {
                return this.adminRepo.GetOne(id);
            }

            return null;
        }

        /// <inheritdoc/>
        public IList<Customer> GetAllCustomers()
        {
            return this.customerRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Administrator> GetAllAdmins()
        {
            return this.adminRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Customer GetCustomer(int id)
        {
            if (this.GetAllCustomers().Contains(this.customerRepo.GetOne(id)))
            {
                return this.customerRepo.GetOne(id);
            }

            return null;
        }

        /// <inheritdoc/>
        public bool UpdateAdminName(int id, string newName)
        {
            if (this.GetAdmin(id) != null)
            {
                this.adminRepo.UpdateName(id, newName);
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateAdminOffice(int id, int newOfficeID)
        {
            if (this.GetAdmin(id) != null)
            {
                this.adminRepo.UpdateOfficeID(id, newOfficeID);
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateAdminRank(int id, string newRank)
        {
            if (this.GetAdmin(id) != null)
            {
                this.adminRepo.UpdateRank(id, newRank);
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateCustomerName(int id, string newName)
        {
            if (this.GetCustomer(id) != null)
            {
                this.customerRepo.UpdateName(id, newName);
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateCustomerPassword(int id, string oldPassword, string newPassword)
        {
            string oldPW = this.customerRepo.GetOne(id).Password;
            int length = 0;

            if (newPassword != null)
            {
                length = newPassword.Length;
            }

            if (oldPW == oldPassword)
            {
                if (length >= 5)
                {
                    this.customerRepo.UpdatePassword(id, oldPassword, newPassword);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public bool UpdateCustomerCity(int id, string newCity)
        {
            if (this.GetCustomer(id) != null)
            {
                this.customerRepo.UpdateCity(id, newCity);
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateCustomerCountry(int id, string newCountry)
        {
            if (this.GetCustomer(id) != null)
            {
                this.customerRepo.UpdateCountry(id, newCountry);
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateCustomerEmail(int id, string newEmail)
        {
            if (this.GetCustomer(id) != null)
            {
                this.customerRepo.UpdateEmail(id, newEmail);
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateAdminEmail(int id, string newEmail)
        {
            if (this.GetAdmin(id) != null)
            {
                this.adminRepo.UpdateEmail(id, newEmail);
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateAdminSalary(int id, int newSalary)
        {
            if (this.GetAdmin(id) != null)
            {
                this.adminRepo.UpdateSalary(id, newSalary);
                return true;
            }

            return false;
        }
    }
}
