﻿// <copyright file="ShopLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using ToyShop.Data;
    using ToyShop.Repository;

    /// <summary>
    /// This class manages the orders.
    /// </summary>
    public class ShopLogic : IShopLogic
    {
        private IOrderRepository orderRepo;
        private IToyRepository toyRepo;
        private IOrderedToyRepository orderedToyRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShopLogic"/> class.
        /// </summary>
        /// <param name="orderRepo"> The repository of the Orders table. </param>
        /// <param name="toyRepo"> The repository of the Toys table. </param>
        /// <param name="orderedToyRepo"> The repository of the OrderedToy table. </param>
        public ShopLogic(IOrderRepository orderRepo, IToyRepository toyRepo, IOrderedToyRepository orderedToyRepo)
        {
            this.orderRepo = orderRepo;
            this.toyRepo = toyRepo;
            this.orderedToyRepo = orderedToyRepo;
        }

        /// <inheritdoc/>
        public void AddOrder(Order newOrder)
        {
            this.orderRepo.Add(newOrder);
        }

        /// <inheritdoc/>
        public void AddToy(Toy newToy)
        {
            this.toyRepo.Add(newToy);
        }

        /// <inheritdoc/>
        public bool AddToyToOrder(int id, Toy toy, int quantity)
        {
            if (toy != null)
            {
                if (this.GetOrder(id) != null)
                {
                    this.orderRepo.AddToyToOrder(id, toy, quantity);
                    this.toyRepo.UpdateQuantity(toy.ID, -quantity);

                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public bool DeleteOrder(int id)
        {
            if (this.GetOrder(id) != null)
            {
                this.orderRepo.Delete(this.GetOrder(id));

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool DeleteToy(int id)
        {
            if (this.GetToy(id) != null)
            {
                this.toyRepo.Delete(this.GetToy(id));
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public IList<Order> GetAllOrders()
        {
            return this.orderRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Toy> GetAllToys()
        {
            return this.toyRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<OrderedToy> GetOrderContent(int id)
        {
            List<OrderedToy> emptyList = new List<OrderedToy>();
            if (this.GetOrder(id) != null)
            {
                var temp = from x in this.orderRepo.GetAll()
                           join y in this.orderedToyRepo.GetAll() on x.ID equals y.OrderID
                           where y.OrderID == id
                           select y;

                return (IList<OrderedToy>)temp.ToList();
            }

            return emptyList;
        }

        /// <summary>
        /// Gets an orders content in async.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        /// <returns>Return a list of ordered toys.</returns>
        public Task<IList<OrderedToy>> GetOrderContentAsync(int id)
        {
            return Task.Run(() => this.GetOrderContent(id));
        }

        /// <inheritdoc/>
        public Order GetOrder(int id)
        {
            if (this.GetAllOrders().Contains(this.orderRepo.GetOne(id)))
            {
                return this.orderRepo.GetOne(id);
            }

            return null;
        }

        /// <inheritdoc/>
        public OrderedToy GetOrderedToy(int id)
        {
            if (this.GetAllOrderedToys().Contains(this.orderedToyRepo.GetOne(id)))
            {
                return this.orderedToyRepo.GetOne(id);
            }

            return null;
        }

        /// <inheritdoc/>
        public Toy GetToy(int id)
        {
            if (this.GetAllToys().Contains(this.toyRepo.GetOne(id)))
            {
                return this.toyRepo.GetOne(id);
            }

            return null;
        }

        /// <inheritdoc/>
        public bool UpdateOrdersAdmin(int orderID, Administrator newAdmin)
        {
            if (newAdmin != null)
            {
                if (this.GetOrder(orderID) != null)
                {
                    this.orderRepo.UpdateOrderAdmin(orderID, newAdmin.ID);

                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateToyName(int id, string newName)
        {
            if (this.GetToy(id) != null)
            {
                this.toyRepo.UpdateName(id, newName);

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateToyPrice(int id, int newPrice)
        {
            if (this.GetToy(id) != null)
            {
                this.toyRepo.UpdatePrice(id, newPrice);

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public double GetAverageNumberOfToysInAllOrders()
        {
            var toyCount = this.orderedToyRepo.GetAll().Count();
            var orderCount = this.orderRepo.GetAll().Count();

            double avg = toyCount / orderCount;

            return avg;
        }

        /// <inheritdoc/>
        public int GetOrderValue(int id)
        {
            if (this.GetOrder(id) != null)
            {
                var temp = from x in this.orderRepo.GetAll()
                           join y in this.orderedToyRepo.GetAll() on x.ID equals y.OrderID
                           where x.ID == id
                           select y;

                int sum = 0;
                foreach (var item in temp)
                {
                    sum += item.Price * item.Quantity;
                }

                return sum;
            }

            return 0;
        }

        /// <summary>
        /// Gets the value of an order in async.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        /// <returns>The value of an order.</returns>
        public Task<int> GetOrderValueAsync(int id)
        {
            return Task.Run(() => this.GetOrderValue(id));
        }

        /// <inheritdoc/>
        public Dictionary<string, int> GetAllOrdersValues()
        {
            Order o = this.GetAllOrders().First();
            int count = this.GetAllOrders().Count;
            int value = 0;
            Dictionary<string, int> temp = new Dictionary<string, int>();
            for (int i = o.ID; i < o.ID + count; i++)
            {
                Order order = null;
                value = 0;
                if (this.GetOrder(i) != null)
                {
                    order = this.GetOrder(i);

                    var q = from x in this.orderRepo.GetAll()
                            join y in this.orderedToyRepo.GetAll() on x.ID equals y.OrderID
                            where order.ID == y.OrderID
                            select y;
                    foreach (var item in q)
                    {
                        value += item.Price * item.Quantity;
                    }

                    temp.Add($"{order.ID}", value);
                }
                else
                {
                    temp.Add($"#{i} order doesn't exist in the database.", 0);
                }
            }

            return temp;
        }

        /// <summary>
        /// Gets all of the orders and their values in a dictionary in async.
        /// </summary>
        /// <returns>String-int dictionary as [ID]-value.</returns>
        public Task<Dictionary<string, int>> GetAllOrdersValuesAsync()
        {
            return Task.Run(() => this.GetAllOrdersValues());
        }

        /// <inheritdoc/>
        public IList<OrderedToy> GetAllOrderedToys()
        {
            return this.orderedToyRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<int> GetAllCustomerIdsWithGivenToyInOrder(int id)
        {
            List<int> emptyList = new List<int>();

            if (this.GetToy(id) != null)
            {
                Toy temptoy = this.GetToy(id);

                var temp = from toy in this.orderedToyRepo.GetAll()
                           join order in this.orderRepo.GetAll() on toy.OrderID equals order.ID
                           where temptoy.ID == toy.ToyID
                           select order.CustomerID;
                return (IList<int>)temp.Distinct().ToList();
            }

            return emptyList;
        }

        /// <summary>
        /// Get a list of customerid's who has the toy(id) in any of his orders in async.
        /// </summary>
        /// <param name="id">The id of the toy.</param>
        /// <returns>An async task type object.</returns>
        public Task<IList<int>> GetAllCustomersWithGivenToyInOrderAsync(int id)
        {
            return Task.Run(() => this.GetAllCustomerIdsWithGivenToyInOrder(id));
        }

        /// <inheritdoc/>
        public bool ChangeToyQuantityInOrder(int toyId, int newQuantity)
        {
            if (this.GetOrderedToy(toyId) != null)
            {
                this.orderedToyRepo.UpdateQuantity(toyId, newQuantity);

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateToyManufacturer(int id, string newManufacturer)
        {
            if (this.GetToy(id) != null)
            {
                this.toyRepo.UpdateManufacturer(id, newManufacturer);

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateToyQuantity(int id, int plusQuantity)
        {
            if (this.GetToy(id) != null)
            {
                this.toyRepo.UpdateQuantity(id, plusQuantity);

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool RemoveToyFromOrder(int orderId, int toyId)
        {
            if (this.GetOrder(orderId) != null)
            {
                if (this.GetOrderedToy(toyId) != null)
                {
                    OrderedToy otoy = this.GetOrderedToy(toyId);
                    Toy toy = this.GetToy(otoy.ToyID);

                    this.orderRepo.RemoveToyFromOrder(toyId);
                    this.toyRepo.UpdateQuantity(toy.ID, otoy.Quantity);

                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc/>
        public bool UpdateToyCategory(int id, string newCategory)
        {
            if (this.GetToy(id) != null)
            {
                this.toyRepo.UpdateToyCategory(id, newCategory);
                return true;
            }

            return false;
        }
    }
}
