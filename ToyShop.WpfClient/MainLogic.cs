﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Main logic class for api.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "http://localhost:1191/ToysApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <inheritdoc/>
        public void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "ToyResult");
        }

        /// <inheritdoc/>
        public List<ToyVM> ApiGetToys()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<ToyVM>>(json, this.jsonOptions);
            return list;
        }

        /// <inheritdoc/>
        public void ApiDelToy(ToyVM toy)
        {
            bool success = false;
            if (toy != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + toy.Id.ToString(CultureInfo.CurrentCulture)).Result;
                JsonDocument doc = JsonDocument.Parse(json);

                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <inheritdoc/>
        public bool ApiEditToy(ToyVM toy, bool isEditing)
        {
            if (toy == null)
            {
                return false;
            }

            string myUrl = isEditing ? this.url + "mod" : this.url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();

            if (!isEditing)
            {
                postData.Add("id", toy.Id.ToString(CultureInfo.CurrentCulture));
                postData.Add("name", toy.Name);
                postData.Add("manufacturer", toy.Manufacturer);
                postData.Add("category", toy.Category);
                postData.Add("quantity", toy.Quantity.ToString(CultureInfo.CurrentCulture));
                postData.Add("price", toy.Price.ToString(CultureInfo.CurrentCulture));
            }
            else
            {
                postData.Add("id", toy.Id.ToString(CultureInfo.CurrentCulture));
                postData.Add("name", toy.Name);
                postData.Add("manufacturer", toy.Manufacturer);
                postData.Add("category", toy.Category);
                postData.Add("quantity", toy.Quantity.ToString(CultureInfo.CurrentCulture));
                postData.Add("price", toy.Price.ToString(CultureInfo.CurrentCulture));
            }

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).
                Result.Content.ReadAsStringAsync().Result;

            JsonDocument doc = JsonDocument.Parse(json);

            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <inheritdoc/>
        public void EditToy(ToyVM toy, Func<ToyVM, bool> editor)
        {
            ToyVM clone = new ToyVM();
            if (toy != null)
            {
                clone.CopyFrom(toy);
            }

            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (toy != null)
                {
                    success = this.ApiEditToy(clone, true);
                }
                else
                {
                    success = this.ApiEditToy(clone, false);
                }
            }

            this.SendMessage(success == true);
        }
    }
}
