﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// MainLogic interface.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// Sends a message indicating whether the operation was successful or not.
        /// </summary>
        /// <param name="success">Operation state.</param>
        void SendMessage(bool success);

        /// <summary>
        /// Gets a collection of toys from the api.
        /// </summary>
        /// <returns>Returns the toy list.</returns>
        List<ToyVM> ApiGetToys();

        /// <summary>
        /// Deletes a toy from the database through api.
        /// </summary>
        /// <param name="toy">The deletable toy.</param>
        void ApiDelToy(ToyVM toy);

        /// <summary>
        /// Edits a toy.
        /// </summary>
        /// <param name="toy">The editable toy.</param>
        /// <param name="isEditing">True if editing, false if adding new.</param>
        /// <returns>Returns true if operation was successful.</returns>
        bool ApiEditToy(ToyVM toy, bool isEditing);

        /// <summary>
        /// Edits a toy entity.
        /// </summary>
        /// <param name="toy">The editable toy.</param>
        /// <param name="editor">The editor function.</param>
        void EditToy(ToyVM toy, Func<ToyVM, bool> editor);
    }
}
