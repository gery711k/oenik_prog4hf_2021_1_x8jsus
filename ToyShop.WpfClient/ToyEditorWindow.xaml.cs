﻿// <copyright file="ToyEditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace ToyShop.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for ToyEditorWindow.xaml.
    /// </summary>
    public partial class ToyEditorWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToyEditorWindow"/> class.
        /// </summary>
        public ToyEditorWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToyEditorWindow"/> class.
        /// </summary>
        /// <param name="toy">Editable toy.</param>
        public ToyEditorWindow(ToyVM toy)
            : this()
        {
            this.DataContext = toy;
        }

        private void OKClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
