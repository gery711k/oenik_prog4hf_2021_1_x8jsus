﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Main view model class.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IMainLogic logic;
        private ToyVM selectedToy;
        private ObservableCollection<ToyVM> allToys;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">The needed logic for the class.</param>
        public MainVM(IMainLogic logic)
        {
            this.logic = logic;

            this.LoadCmd = new RelayCommand(() => this.AllToys = new ObservableCollection<ToyVM>(this.logic.ApiGetToys()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelToy(this.selectedToy));
            this.AddCmd = new RelayCommand(() => this.logic.EditToy(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditToy(this.selectedToy, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets... All toys in the viewmodel.
        /// </summary>
        public ObservableCollection<ToyVM> AllToys
        {
            get { return this.allToys; }
            set { this.Set(ref this.allToys, value); }
        }

        /// <summary>
        /// Gets or sets... The selected toy in the vm.
        /// </summary>
        public ToyVM SelectedToy
        {
            get { return this.selectedToy; }
            set { this.Set(ref this.selectedToy, value); }
        }

        /// <summary>
        /// Gets or sets... Editor function for editing a toy.
        /// </summary>
        public Func<ToyVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets... Adding command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets... Deleting command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets... Modifying command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets... Loading command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
