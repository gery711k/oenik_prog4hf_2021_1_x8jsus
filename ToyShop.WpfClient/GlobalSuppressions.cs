﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "<Still no idea what this is.>", Scope = "type", Target = "~T:ToyShop.WpfClient.MainLogic")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<It should return a list right?  RIGHT?>", Scope = "member", Target = "~M:ToyShop.WpfClient.MainLogic.ApiGetToys~System.Collections.Generic.List{ToyShop.WpfClient.ToyVM}")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<BUT I DON'T WANT TO>", Scope = "member", Target = "~M:ToyShop.WpfClient.MainLogic.ApiGetToys~System.Collections.Generic.List{ToyShop.WpfClient.ToyVM}")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<I STILL DON'T WANT TO>", Scope = "member", Target = "~M:ToyShop.WpfClient.MainLogic.ApiDelToy(ToyShop.WpfClient.ToyVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<COME ON ALREADY>", Scope = "member", Target = "~M:ToyShop.WpfClient.MainLogic.ApiEditToy(ToyShop.WpfClient.ToyVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<NO>", Scope = "member", Target = "~M:ToyShop.WpfClient.MainLogic.ApiEditToy(ToyShop.WpfClient.ToyVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<We might need to change the whole list.>", Scope = "member", Target = "~P:ToyShop.WpfClient.MainVM.AllToys")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<NikGitStats: True>", Scope = "member", Target = "~M:ToyShop.WpfClient.IMainLogic.ApiGetToys~System.Collections.Generic.List{ToyShop.WpfClient.ToyVM}")]
