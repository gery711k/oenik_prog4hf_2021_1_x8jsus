﻿// <copyright file="Factory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;

    /// <summary>
    /// Factory class for creating instance to work with database.
    /// </summary>
    public static class Factory
    {
        /// <summary>
        /// Loads the database.
        /// </summary>
        public static void LoadDB()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);

            MyIoc.Instance.Register<IMainLogic, MainLogic>();
        }
    }
}
