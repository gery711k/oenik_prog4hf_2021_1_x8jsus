﻿// <copyright file="ShopLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace ToyShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using ToyShop.Data;
    using ToyShop.Logic;
    using ToyShop.Repository;

    /// <summary>
    /// Shop logic tester class.
    /// </summary>
    [TestFixture]
    public class ShopLogicTests
    {
        private Mock<IOrderRepository> mockedOrderRepo;
        private Mock<IToyRepository> mockedToyRepo;
        private Mock<IOrderedToyRepository> mockedOrderedToyRepo;

        private List<Order> orders;
        private List<OrderedToy> orderedToys;
        private List<Toy> toys;

        private ShopLogic logic;

        /// <summary>
        /// Creates the mocked repositories, logic, and the fake database tables.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            // Arrange
            this.mockedOrderRepo = new Mock<IOrderRepository>(MockBehavior.Loose);
            this.mockedToyRepo = new Mock<IToyRepository>(MockBehavior.Loose);
            this.mockedOrderedToyRepo = new Mock<IOrderedToyRepository>(MockBehavior.Loose);

            this.logic = new ShopLogic(this.mockedOrderRepo.Object, this.mockedToyRepo.Object, this.mockedOrderedToyRepo.Object);

            this.orders = new List<Order>();
            this.orderedToys = new List<OrderedToy>();
            this.toys = new List<Toy>();

            this.toys.Add(new Toy() { ID = 0, Name = "Toy1", Manufacturer = "Manu1", Category = "Category1", Price = 2000, Quantity = 20 });
            this.toys.Add(new Toy() { ID = 1, Name = "Toy2", Manufacturer = "Manu1", Category = "Category1", Price = 1500, Quantity = 20 });
            this.toys.Add(new Toy() { ID = 2, Name = "Toy3", Manufacturer = "Manu2", Category = "Category1", Price = 1000, Quantity = 20 });
            this.toys.Add(new Toy() { ID = 3, Name = "Toy4", Manufacturer = "Manu2", Category = "Category2", Price = 1200, Quantity = 20 });

            this.orderedToys.Add(new OrderedToy() { ID = 0, OrderID = 0, ToyID = 0, Price = 2000, Quantity = 1 });
            this.orderedToys.Add(new OrderedToy() { ID = 1, OrderID = 0, ToyID = 1, Price = 1500, Quantity = 1 });
            this.orderedToys.Add(new OrderedToy() { ID = 2, OrderID = 1, ToyID = 2, Price = 1000, Quantity = 1 });
            this.orderedToys.Add(new OrderedToy() { ID = 3, OrderID = 1, ToyID = 3, Price = 2000, Quantity = 1 });
            this.orderedToys.Add(new OrderedToy() { ID = 4, OrderID = 2, ToyID = 1, Price = 1500, Quantity = 1 });
            this.orderedToys.Add(new OrderedToy() { ID = 5, OrderID = 2, ToyID = 3, Price = 1200, Quantity = 1 });
            this.orderedToys.Add(new OrderedToy() { ID = 6, OrderID = 3, ToyID = 1, Price = 1500, Quantity = 1 });
            this.orderedToys.Add(new OrderedToy() { ID = 7, OrderID = 3, ToyID = 2, Price = 1000, Quantity = 1 });

            this.orders.Add(new Order() { ID = 0, CustomerID = 0, OrderDate = new DateTime(2020, 02, 10, 10, 22, 44) });
            this.orders.Add(new Order() { ID = 1, CustomerID = 1, OrderDate = new DateTime(2020, 02, 10, 10, 22, 44) });
            this.orders.Add(new Order() { ID = 2, CustomerID = 2, OrderDate = new DateTime(2020, 02, 10, 10, 22, 44) });
            this.orders.Add(new Order() { ID = 3, CustomerID = 3, OrderDate = new DateTime(2020, 02, 10, 10, 22, 44) });
        }

        /// <summary>
        /// Tests shoplogic's GetOneToy method.
        /// </summary>
        /// <param name="id">The id of the toy.</param>
        [TestCase(0)]
        [TestCase(1)]
        public void TestGetOneToy(int id)
        {
            // Arrange
            this.mockedToyRepo.Setup(repo => repo.GetOne(It.Is<int>(id => id >= 0 && id < this.toys.Count))).Returns(this.toys[id]);

            // Act
            var result = this.logic.GetToy(id);

            // Assert
            this.mockedToyRepo.Verify(x => x.GetOne(id), Times.Once);
        }

        /// <summary>
        /// Tests shoplogic's GetOneToy method.
        /// </summary>
        /// <param name="id">The id of the toy.</param>
        [TestCase(-1)]
        [TestCase(10)]
        public void TestGetOneToyIdIsOutOfRange(int id)
        {
            this.mockedToyRepo.Setup(repo => repo.GetOne(It.Is<int>(id => id <= 0 && id > this.toys.Count))).Throws<IndexOutOfRangeException>();

            var result = this.logic.GetToy(id);

            this.mockedToyRepo.Verify(x => x.GetOne(id), Times.Once);
        }

        /// <summary>
        /// Tests shoplogic's AddOrder method.
        /// </summary>
        [Test]
        public void TestAddNewOrder()
        {
            // Arrange
            Order order = new Order() { ID = 4, OrderDate = new DateTime(2020, 02, 10, 10, 22, 44) };

            List<Order> expected = this.orders;
            expected.Add(order);

            this.mockedOrderRepo.Setup(repo => repo.Add(order));
            this.mockedOrderRepo.Setup(repo => repo.GetAll()).Returns(this.orders.AsQueryable());

            // Act
            this.logic.AddOrder(order);
            IList<Order> result = this.logic.GetAllOrders();

            // Assert
            Assert.That(result.Count, Is.EqualTo(expected.Count));
            Assert.That(result, Is.EquivalentTo(expected));

            this.mockedOrderRepo.Verify(x => x.GetAll(), Times.Once);
            this.mockedOrderRepo.Verify(x => x.Add(order), Times.Once);
        }

        /// <summary>
        /// Tests shoplogic's UpdateOrderedToyPrice method.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        /// <param name="newQty">The new quantity of the ordered toy.</param>
        [TestCase(0, 20)]
        [TestCase(0, 18)]
        [TestCase(1, 2)]
        public void TestUpdateOrderedToyPrice(int id, int newQty)
        {
            // Arrange
            OrderedToy expected = this.orderedToys[id];
            expected.Quantity = newQty;

            this.mockedOrderedToyRepo.Setup(repo => repo.GetOne(id)).Returns(expected);
            this.mockedOrderedToyRepo.Setup(repo => repo.GetAll()).Returns(this.orderedToys.AsQueryable());
            this.mockedOrderedToyRepo.Setup(repo => repo.UpdateQuantity(id, newQty));

            // Act
            this.logic.ChangeToyQuantityInOrder(id, newQty);
            int result = this.logic.GetOrderedToy(id).Price;

            // Assert
            this.mockedOrderedToyRepo.Verify(x => x.UpdateQuantity(id, newQty), Times.Once);
            Assert.That(result, Is.EqualTo(expected.Price));
        }

        /// <summary>
        /// Tests shoplogic's UpdateOrderedToyPrice method.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        /// <param name="newQty">The new quantity of the ordered toy.</param>
        [TestCase(20, 1010)]
        [TestCase(-1, 21)]
        [TestCase(-2, 25)]
        public void TestUpdateOrderedToyPriceIdIsOutOfRange(int id, int newQty)
        {
            // Arrange
            this.mockedOrderedToyRepo.Setup(repo => repo.GetOne(It.Is<int>(id => id <= 0 && id > this.orderedToys.Count))).Throws<IndexOutOfRangeException>();
            this.mockedOrderedToyRepo.Setup(repo => repo.GetAll()).Returns(this.orderedToys.AsQueryable());
            this.mockedOrderedToyRepo.Setup(repo => repo.UpdateQuantity(It.Is<int>(id => id <= 0 && id > this.orderedToys.Count), newQty)).Throws<IndexOutOfRangeException>();

            // Act
            this.logic.ChangeToyQuantityInOrder(id, newQty);

            // Assert
            this.mockedOrderedToyRepo.Verify(x => x.UpdateQuantity(id, newQty), Times.Never);
        }

        /// <summary>
        /// Tests shoplogic's DeleteOrder method.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        public void TestDeleteOrder(int id)
        {
            // Arrange
            Order order = this.orders[id];

            this.mockedOrderRepo.Setup(repo => repo.GetOne(id)).Returns(order);
            this.mockedOrderRepo.Setup(repo => repo.Delete(order));
            this.mockedOrderRepo.Setup(repo => repo.GetAll()).Returns(this.orders.AsQueryable());

            // Act
            this.logic.DeleteOrder(id);

            // Verify
            this.mockedOrderRepo.Verify(x => x.Delete(order), Times.Once);
        }

        /// <summary>
        /// Tests shoplogic's DeleteOrder method.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        [TestCase(-5)]
        [TestCase(-200)]
        [TestCase(200)]
        public void TestDeleteOrderIdIsOutOfRange(int id)
        {
            // Arrange
            Order order = new Order() { ID = -5 };

            this.mockedOrderRepo.Setup(repo => repo.GetOne(It.Is<int>(id => id <= 0 && id > this.orders.Count))).Throws<IndexOutOfRangeException>();
            this.mockedOrderRepo.Setup(repo => repo.Delete(order)).Throws<IndexOutOfRangeException>();
            this.mockedOrderRepo.Setup(repo => repo.GetAll()).Returns(this.orders.AsQueryable());

            // Act
            this.logic.DeleteOrder(id);

            // Verify
            this.mockedOrderRepo.Verify(x => x.Delete(order), Times.Never);
        }

        /// <summary>
        /// Tests shoplogic's GetAllToys method.
        /// </summary>
        [Test]
        public void TestGetAllToys()
        {
            // Arrange
            this.mockedToyRepo.Setup(repo => repo.GetAll()).Returns(this.toys.AsQueryable());

            // Act
            var result = this.logic.GetAllToys();

            // Verify
            this.mockedToyRepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests shoplogic's GetOrderContent method.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        [TestCase(0)]
        [TestCase(1)]
        public void TestGetOrderContent(int id)
        {
            // Arrange
            List<OrderedToy> expected = new List<OrderedToy>();
            foreach (var item in this.orderedToys)
            {
                if (item.OrderID == id)
                {
                    expected.Add(item);
                }
            }

            this.mockedOrderRepo.Setup(repo => repo.GetOne(id)).Returns(this.orders[id]).Verifiable();
            this.mockedOrderRepo.Setup(repo => repo.GetAll()).Returns(this.orders.AsQueryable()).Verifiable();
            this.mockedOrderedToyRepo.Setup(repo => repo.GetAll()).Returns(this.orderedToys.AsQueryable()).Verifiable();

            // Act
            List<OrderedToy> result = (List<OrderedToy>)this.logic.GetOrderContent(id);

            // Assert
            Assert.That(result, Is.Not.Empty);
            Assert.That(expected, Is.EquivalentTo(result));
            Assert.That(expected[0], Is.EqualTo(result[0]));

            this.mockedOrderedToyRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedOrderRepo.Verify(repo => repo.GetAll(), Times.Exactly(2));
            this.mockedOrderRepo.Verify(repo => repo.GetOne(id), Times.Exactly(2));
        }

        /// <summary>
        /// Tests shoplogic's GetOrderContent method.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        [TestCase(-1)]
        [TestCase(10)]
        public void TestGetOrderContentThrowsException(int id)
        {
            this.mockedOrderRepo.Setup(repo => repo.GetOne(It.Is<int>(id => id <= 0 && id > this.orders.Count))).Throws(new IndexOutOfRangeException());

            // Act
            List<OrderedToy> result = (List<OrderedToy>)this.logic.GetOrderContent(id);

            this.mockedOrderRepo.Verify(repo => repo.GetOne(id), Times.Once);
        }

        /// <summary>
        /// Tests the shoplogic's GetAllOrdersValues method.
        /// </summary>
        [Test]
        public void TestGetAllOrdersValues()
        {
            // Arrange
            Order o = this.orders.First();
            int count = this.orders.Count;
            int value = 0;

            Dictionary<string, int> expected = new Dictionary<string, int>();
            for (int i = o.ID; i < o.ID + count; i++)
            {
                this.mockedOrderRepo.Setup(repo => repo.GetOne(i)).Returns(this.orders[i]);
                value = 0;
                Order order = this.orders[i];

                var q = from x in this.orders
                        join y in this.orderedToys on x.ID equals y.OrderID
                        where order.ID == y.OrderID
                        select y;

                foreach (var item in q)
                {
                    value += item.Price * item.Quantity;
                }

                expected.Add($"{order.ID}", value);
            }

            this.mockedOrderRepo.Setup(repo => repo.GetAll()).Returns(this.orders.AsQueryable()).Verifiable();
            this.mockedOrderedToyRepo.Setup(repo => repo.GetAll()).Returns(this.orderedToys.AsQueryable()).Verifiable();

            // Act
            Dictionary<string, int> result = new Dictionary<string, int>(this.logic.GetAllOrdersValues());

            // Assert
            Assert.That(result, Is.Not.Empty);
            Assert.That(result.Last(), Is.EqualTo(expected.Last()));
            Assert.That(result.First(), Is.EqualTo(expected.First()));
            Assert.That(result, Is.EquivalentTo(expected));

            int getallruntimes = 6 + (this.orders.Count * 2);
            int getoneruntimes = this.orders.Count * 4;

            this.mockedOrderRepo.Verify(repo => repo.GetAll(), Times.Exactly(getallruntimes));
            this.mockedOrderRepo.Verify(repo => repo.GetOne(It.Is<int>(id => id >= 0)), Times.Exactly(getoneruntimes));
        }

        /// <summary>
        /// Tests the shoplogic's GetAllCustomerIdsWithGivenToyInOrder method.
        /// </summary>
        /// <param name="id">The id of the toy.</param>
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        public void TestGetAllCustomersIdsWithGivenToyInOrder(int id)
        {
            // Arrange
            List<int> expected = new List<int>();

            Toy temptoy = this.toys[id];

            var temp = from toy in this.orderedToys
                       join order in this.orders on toy.OrderID equals order.ID
                       where temptoy.ID == toy.ToyID
                       select order.CustomerID;

            foreach (var item in temp)
            {
                expected.Add(item);
            }

            this.mockedToyRepo.Setup(repo => repo.GetOne(It.Is<int>(id => id >= 0 && id < this.toys.Count))).Returns(this.toys[id]);
            this.mockedToyRepo.Setup(repo => repo.GetAll()).Returns(this.toys.AsQueryable());
            this.mockedOrderedToyRepo.Setup(repo => repo.GetAll()).Returns(this.orderedToys.AsQueryable()).Verifiable();
            this.mockedOrderRepo.Setup(repo => repo.GetAll()).Returns(this.orders.AsQueryable());

            // Act
            List<int> result = new List<int>();

            result = this.logic.GetAllCustomerIdsWithGivenToyInOrder(id).ToList();

            // Assert
            this.mockedToyRepo.Verify(repo => repo.GetOne(id), Times.Exactly(4));
            this.mockedOrderRepo.Verify(repo => repo.GetAll(), Times.Exactly(1));
            this.mockedOrderedToyRepo.Verify(repo => repo.GetAll(), Times.Exactly(1));

            Assert.That(result, Is.Not.Empty);
            Assert.That(result.First(), Is.EqualTo(expected.First()));
            Assert.That(result.Last(), Is.EqualTo(expected.Last()));
            Assert.That(result, Is.EquivalentTo(expected));
        }
    }
}