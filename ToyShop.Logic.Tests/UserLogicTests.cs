﻿// <copyright file="UserLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using ToyShop.Data;
    using ToyShop.Logic;
    using ToyShop.Repository;

    /// <summary>
    /// User logic tester class.
    /// </summary>
    [TestFixture]
    public class UserLogicTests
    {
        private Mock<IAdminRepository> mockedAdminRepo;
        private Mock<ICustomerRepository> mockedCustomerRepo;

        private List<Administrator> admins;
        private List<Customer> customers;

        private UserLogic logic;

        /// <summary>
        ///  Creates the mocked repositories, logic, and the fake database tables.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockedAdminRepo = new Mock<IAdminRepository>(MockBehavior.Loose);
            this.mockedCustomerRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);

            this.logic = new UserLogic(this.mockedAdminRepo.Object, this.mockedCustomerRepo.Object);

            this.admins = new List<Administrator>();
            this.customers = new List<Customer>();

            this.admins.Add(new Administrator() { ID = 0, Name = "Admin1", Email = "AdminEmail1", Rank = "Rank1", OfficeID = 1, Salary = 5000 });
            this.admins.Add(new Administrator() { ID = 1, Name = "Admin2", Email = "AdminEmail2", Rank = "Rank2", OfficeID = 1, Salary = 4000 });
            this.admins.Add(new Administrator() { ID = 2, Name = "Admin3", Email = "AdminEmail4", Rank = "Rank2", OfficeID = 1, Salary = 3900 });
            this.admins.Add(new Administrator() { ID = 3, Name = "Admin4", Email = "AdminEmail4", Rank = "Rank3", OfficeID = 2, Salary = 2000 });

            this.customers.Add(new Customer() { ID = 0, Name = "Cust1", Email = "CustMail1", City = "City1", Country = "Country1", Password = "pw1" });
            this.customers.Add(new Customer() { ID = 1, Name = "Cust2", Email = "CustMail2", City = "City2", Country = "Country2", Password = "pw2" });
            this.customers.Add(new Customer() { ID = 2, Name = "Cust3", Email = "CustMail3", City = "City2", Country = "Country1", Password = "pw3" });
            this.customers.Add(new Customer() { ID = 3, Name = "Cust4", Email = "CustMail4", City = "City3", Country = "Country2", Password = "pw4" });
        }

        /// <summary>
        /// Tests userlogic's DeleteAdmin method.
        /// </summary>
        /// <param name="id">The id of the admin.</param>
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        public void TestDeleteAdmin(int id)
        {
            // Arrange
            Administrator admin = this.admins[id];

            this.mockedAdminRepo.Setup(repo => repo.GetOne(id)).Returns(admin);
            this.mockedAdminRepo.Setup(repo => repo.Delete(admin));
            this.mockedAdminRepo.Setup(repo => repo.GetAll()).Returns(this.admins.AsQueryable());

            // Act
            this.logic.DeleteAdmin(id);

            // Verify
            this.mockedAdminRepo.Verify(x => x.Delete(admin), Times.Once);
        }

        /// <summary>
        /// Tests userlogic's GetAllCustomer method.
        /// </summary>
        [Test]
        public void TestGetAllCustomers()
        {
            // Arrange
            this.mockedCustomerRepo.Setup(repo => repo.GetAll()).Returns(this.customers.AsQueryable());

            // Act
            var result = this.logic.GetAllCustomers();

            // Verify
            this.mockedCustomerRepo.Verify(x => x.GetAll(), Times.Once);
        }
    }
}
