﻿// <copyright file="AdminUI.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This is the ui alternative of the Administrator class.
    /// </summary>
    public class AdminUI : ObservableObject
    {
        private int id;
        private string name;
        private string rank;
        private int officeID;
        private string email;
        private int salary;

        /// <summary>
        /// Gets or sets the id of the AdminUI entity.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets the name of the AdminUI entity.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the rank of the AdminUI entity.
        /// </summary>
        public string Rank
        {
            get { return this.rank; }
            set { this.Set(ref this.rank, value); }
        }

        /// <summary>
        /// Gets or sets the officeid of the AdminUI entity.
        /// </summary>
        public int OfficeID
        {
            get { return this.officeID; }
            set { this.Set(ref this.officeID, value); }
        }

        /// <summary>
        /// Gets or sets the email of the AdminUI entity.
        /// </summary>
        public string Email
        {
            get { return this.email; }
            set { this.Set(ref this.email, value); }
        }

        /// <summary>
        /// Gets or sets the salary of the AdminUI entity.
        /// </summary>
        public int Salary
        {
            get { return this.salary; }
            set { this.Set(ref this.salary, value); }
        }

        /// <summary>
        /// Copies an AdminUI entity for a working copy.
        /// </summary>
        /// <param name="admin">AdminUI entity.</param>
        public void CopyFrom(AdminUI admin)
        {
            this.GetType().
                GetProperties().
                ToList().
                ForEach(
                    property => property.SetValue(
                        this, property.GetValue(admin)));
        }
    }
}
