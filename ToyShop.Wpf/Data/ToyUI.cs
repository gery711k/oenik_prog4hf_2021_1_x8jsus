﻿// <copyright file="ToyUI.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This is the ui alternative of the Toy class.
    /// </summary>
    public class ToyUI : ObservableObject
    {
        private int id;
        private string name;
        private string manufacturer;
        private int price;
        private string category;
        private int quantity;

        /// <summary>
        /// Gets or sets the id of the ToyUI entity.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets the name of the ToyUI entity.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the manufacturer of the ToyUI entity.
        /// </summary>
        public string Manufacturer
        {
            get { return this.manufacturer; }
            set { this.Set(ref this.manufacturer, value); }
        }

        /// <summary>
        /// Gets or sets the price of the ToyUI entity.
        /// </summary>
        public int Price
        {
            get { return this.price; }
            set { this.Set(ref this.price, value); }
        }

        /// <summary>
        /// Gets or sets the category of the ToyUI entity.
        /// </summary>
        public string Category
        {
            get { return this.category; }
            set { this.Set(ref this.category, value); }
        }

        /// <summary>
        /// Gets or sets the quantity of the ToyUI entity.
        /// </summary>
        public int Quantity
        {
            get { return this.quantity; }
            set { this.Set(ref this.quantity, value); }
        }

        /// <summary>
        /// Copies a ToyUI entity for a working copy.
        /// </summary>
        /// <param name="toy">ToyUI entity.</param>
        public void CopyFrom(ToyUI toy)
        {
            this.GetType().GetProperties().ToList().ForEach(
                property => property.SetValue(this, property.GetValue(toy)));
        }
    }
}
