﻿// <copyright file="CustomerUI.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// This is the ui alternative of the Customer class.
    /// </summary>
    public class CustomerUI : ObservableObject
    {
        private int id;
        private string name;
        private string password;
        private string email;
        private string country;
        private string city;

        /// <summary>
        /// Gets or sets the id of the CustomerUI entity.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets the name of the CustomerUI entity.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the email  of the CustomerUI entity.
        /// </summary>
        public string Email
        {
            get { return this.email; }
            set { this.Set(ref this.email, value); }
        }

        /// <summary>
        /// Gets or sets the country of the CustomerUI entity.
        /// </summary>
        public string Country
        {
            get { return this.country; }
            set { this.Set(ref this.country, value); }
        }

        /// <summary>
        /// Gets or sets the city of the CustomerUI entity.
        /// </summary>
        public string City
        {
            get { return this.city; }
            set { this.Set(ref this.city, value); }
        }

        /// <summary>
        /// Gets or sets the password of the CustomerUI entity.
        /// </summary>
        public string Password
        {
            get { return this.password; }
            set { this.Set(ref this.password, value); }
        }

        /// <summary>
        /// Copies an CustomerUI entity for a working copy.
        /// </summary>
        /// <param name="customer">CustomerUI entity.</param>
        public void CopyFrom(CustomerUI customer)
        {
            this.GetType().
                GetProperties().
                ToList().
                ForEach(
                    property => property.SetValue(
                        this, property.GetValue(customer)));
        }
    }
}
