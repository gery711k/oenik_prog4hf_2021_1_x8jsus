﻿// <copyright file="AdminViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using ToyShop.Wpf.BL;
    using ToyShop.Wpf.Data;

    /// <summary>
    /// The admin tab's viewmodel.
    /// </summary>
    public class AdminViewModel : ViewModelBase
    {
        private IUILogic<AdminUI> logic;
        private AdminUI selectedAdmin;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic of the window.</param>
        public AdminViewModel(IUILogic<AdminUI> logic)
        {
            this.logic = logic;
            this.Admins = new ObservableCollection<AdminUI>();
            this.SetScene();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminViewModel"/> class.
        /// </summary>
        public AdminViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IUILogic<AdminUI>>())
        {
        }

        /// <summary>
        /// Gets a list of adminui entities.
        /// </summary>
        public ObservableCollection<AdminUI> Admins { get; private set; }

        /// <summary>
        /// Gets or sets the currently selected admin.
        /// </summary>
        public AdminUI SelectedAdmin
            {
            get { return this.selectedAdmin; }
            set { this.Set(ref this.selectedAdmin, value); }
            }

        /// <summary>
        /// Gets... Invokes the dblogic's add method for the addable entity.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets... Invokes the dblogic's modify methods for the modifiable entity.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets... Invokes the dblogic's delete method for the deletable entity.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        private void SetScene()
        {
            this.SetupButtons();

            if (this.IsInDesignMode)
            {
                AdminUI a1 = new AdminUI()
                {
                    Name = "name-1",
                    Email = "email-1",
                    OfficeID = 1,
                    Rank = "rank-1",
                    Salary = 1000,
                };
                AdminUI a2 = new AdminUI()
                {
                    Name = "name-2",
                    Email = "email-2",
                    OfficeID = 2,
                    Rank = "rank-2",
                    Salary = 2000,
                };

                this.Admins.Add(a1);
                this.Admins.Add(a2);
            }
            else
            {
                this.logic.GetAll().ToList().ForEach(x => this.Admins.Add(x));
            }
        }

        private void SetupButtons()
        {
            this.AddCmd = new RelayCommand(() => this.logic.Add(this.Admins));
            this.ModCmd = new RelayCommand(() => this.logic.Modify(this.selectedAdmin));
            this.DelCmd = new RelayCommand(() => this.logic.Delete(this.Admins, this.selectedAdmin));
        }
    }
}
