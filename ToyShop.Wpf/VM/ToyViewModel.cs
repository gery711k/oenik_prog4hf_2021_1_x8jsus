﻿// <copyright file="ToyViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using ToyShop.Wpf.BL;
    using ToyShop.Wpf.Data;

    /// <summary>
    /// The toy tab's viewmodel.
    /// </summary>
    public class ToyViewModel : ViewModelBase
    {
        private IUILogic<ToyUI> logic;
        private ToyUI selectedToy;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToyViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic of the window.</param>
        public ToyViewModel(IUILogic<ToyUI> logic)
        {
            this.logic = logic;
            this.Toys = new ObservableCollection<ToyUI>();
            this.SetScene();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToyViewModel"/> class.
        /// </summary>
        public ToyViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IUILogic<ToyUI>>())
        {
        }

        /// <summary>
        /// Gets a list of toyui entities.
        /// </summary>
        public ObservableCollection<ToyUI> Toys { get; private set; }

        /// <summary>
        /// Gets or sets the currently selected toy.
        /// </summary>
        public ToyUI SelectedToy
        {
            get { return this.selectedToy; }
            set { this.Set(ref this.selectedToy, value); }
        }

        /// <summary>
        /// Gets... Invokes the dblogic's add method for the addable entity.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets... Invokes the dblogic's modify methods for the modifiable entity.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets... Invokes the dblogic's delete method for the deletable entity.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        private void SetupButtons()
        {
            this.AddCmd = new RelayCommand(() => this.logic.Add(this.Toys));
            this.ModCmd = new RelayCommand(() => this.logic.Modify(this.selectedToy));
            this.DelCmd = new RelayCommand(() => this.logic.Delete(this.Toys, this.selectedToy));
        }

        private void SetScene()
        {
            this.SetupButtons();

            if (this.IsInDesignMode)
            {
                ToyUI toy1 = new ToyUI()
                {
                    Name = "toy-1",
                    Manufacturer = "manu-1",
                    Category = "cat-1",
                    Price = 1000,
                    Quantity = 10,
                };
                ToyUI toy2 = new ToyUI()
                {
                    Name = "toy-2",
                    Manufacturer = "manu-1",
                    Category = "cat-2",
                    Price = 1500,
                    Quantity = 10,
                };

                this.Toys.Add(toy1);
                this.Toys.Add(toy2);
            }
            else
            {
                this.logic.GetAll().ToList().ForEach(x => this.Toys.Add(x));
            }
        }
    }
}
