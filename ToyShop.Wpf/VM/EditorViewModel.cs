﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using ToyShop.Wpf.Data;

    /// <summary>
    /// The editor window's viewmodel.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private object entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.SetType();
            this.SetTestScene();
        }

        /// <summary>
        /// Gets or sets the editable entity(ui).
        /// </summary>
        public object Entity
        {
            get { return this.entity; }
            set { this.Set(ref this.entity, value); }
        }

        private void SetType()
        {
            if (this.entity is ToyUI)
            {
                this.entity = new ToyUI();
            }
            else if (this.entity is AdminUI)
            {
                this.entity = new AdminUI();
            }
            else
            {
                this.entity = new CustomerUI();
            }
        }

        private void SetTestScene()
        {
            if (this.IsInDesignMode)
            {
                if (this.entity is ToyUI)
                {
                    (this.entity as ToyUI).Name = "JATEK";
                    (this.entity as ToyUI).Manufacturer = "LEGO";
                    (this.entity as ToyUI).Category = "KIDS";
                    (this.entity as ToyUI).Price = 1000;
                    (this.entity as ToyUI).Quantity = 10;
                }
                else if (this.entity is AdminUI)
                {
                    (this.entity as AdminUI).Name = "Sanyi";
                    (this.entity as AdminUI).Email = "sanyi@mail.hu";
                    (this.entity as AdminUI).OfficeID = 1;
                    (this.entity as AdminUI).Salary = 1000;
                    (this.entity as AdminUI).Rank = "rank";
                }
                else
                {
                    (this.entity as CustomerUI).Name = "Sanyi";
                    (this.entity as CustomerUI).Email = "sanyi@mail.hu";
                    (this.entity as CustomerUI).City = "City";
                    (this.entity as CustomerUI).Country = "Country";
                }
            }
        }
    }
}
