﻿// <copyright file="CustomerViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using ToyShop.Wpf.BL;
    using ToyShop.Wpf.Data;

    /// <summary>
    /// The customer tab's viewmodel.
    /// </summary>
    public class CustomerViewModel : ViewModelBase
    {
        private IUILogic<CustomerUI> logic;
        private CustomerUI selectedCustomer;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerViewModel"/> class.
        /// </summary>
        /// <param name="logic">The logic of the window.</param>
        public CustomerViewModel(IUILogic<CustomerUI> logic)
        {
            this.logic = logic;
            this.Customers = new ObservableCollection<CustomerUI>();
            this.SetScene();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerViewModel"/> class.
        /// </summary>
        public CustomerViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IUILogic<CustomerUI>>())
        {
        }

        /// <summary>
        /// Gets a list of customerui entities.
        /// </summary>
        public ObservableCollection<CustomerUI> Customers { get; private set; }

        /// <summary>
        /// Gets or sets the currently selected admin.
        /// </summary>
        public CustomerUI SelectedCustomer
        {
            get { return this.selectedCustomer; }
            set { this.Set(ref this.selectedCustomer, value); }
        }

        /// <summary>
        /// Gets... Invokes the dblogic's add method for the addable entity.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets... Invokes the dblogic's modify methods for the modifiable entity.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets... Invokes the dblogic's delete method for the deletable entity.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        private void SetupButtons()
        {
            this.AddCmd = new RelayCommand(() => this.logic.Add(this.Customers));
            this.ModCmd = new RelayCommand(() => this.logic.Modify(this.selectedCustomer));
            this.DelCmd = new RelayCommand(() => this.logic.Delete(this.Customers, this.selectedCustomer));
        }

        private void SetScene()
        {
            this.SetupButtons();

            if (this.IsInDesignMode)
            {
                CustomerUI c1 = new CustomerUI()
                {
                    Name = "name-1",
                    Email = "email-1",
                    City = "city-1",
                    Country = "country-1",
                    Password = "hidden",
                };
                CustomerUI c2 = new CustomerUI()
                {
                    Name = "name-2",
                    Email = "email-2",
                    City = "city-2",
                    Country = "country-2",
                    Password = "hidden",
                };

                this.Customers.Add(c1);
                this.Customers.Add(c2);
            }
            else
            {
                this.logic.GetAll().ToList().ForEach(x => this.Customers.Add(x));
            }
        }
    }
}
