﻿// <copyright file="AdminLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using ToyShop.Data;
    using ToyShop.Logic;
    using ToyShop.Wpf.Data;

    /// <summary>
    /// Admin logic for ui.
    /// </summary>
    public class AdminLogic : IUILogic<AdminUI>
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private IUserLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminLogic"/> class.
        /// </summary>
        /// <param name="editorService">Editorservice interface.</param>
        /// <param name="messengerService">Messenger service interface.</param>
        /// <param name="logic">Database logic class.</param>
        public AdminLogic(IEditorService editorService, IMessenger messengerService, IUserLogic logic)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.logic = logic;
        }

        /// <inheritdoc/>
        public void Add(IList<AdminUI> entityList)
        {
            AdminUI newAdmin = new AdminUI();

            if (this.editorService.EditAdmin(newAdmin) == true)
            {
                entityList?.Add(newAdmin);
                this.AddToDatabase(newAdmin);
                entityList.Last().Id = this.logic.GetAllAdmins().Last().ID;
                this.messengerService.Send("NEW ADMIN WAS SUCCESSFULLY ADDED!", "+LogicResult");
            }
            else
            {
                this.messengerService.Send("ADDING NEW ADMIN WAS CANCELLED!", "-LogicResult");
            }
        }

        /// <inheritdoc/>
        public void Delete(IList<AdminUI> entityList, AdminUI entityToDelete)
        {
            if (entityToDelete != null && entityList != null && entityList.Remove(entityToDelete))
            {
                this.logic.DeleteAdmin(entityToDelete.Id);
                this.messengerService.Send("ADMIN WAS SUCCESSFULLY DELETED!", "+LogicResult");
            }
            else
            {
                this.messengerService.Send("YOU DON'T HAVE ANYTHING SELECTED!", "?LogicResult");
            }
        }

        /// <inheritdoc/>
        public IList<AdminUI> GetAll()
        {
            List<Administrator> admins = this.logic.GetAllAdmins().ToList();
            List<AdminUI> list = new List<AdminUI>();

            foreach (var newAdmin in admins)
            {
                list.Add(ConvertFromDatabase(newAdmin));
            }

            return list;
        }

        /// <inheritdoc/>
        public void Modify(AdminUI entityToModify)
        {
            if (entityToModify == null)
            {
                this.messengerService.Send("YOU DON'T HAVE ANYTHING SELECTED!", "?LogicResult");
                return;
            }

            AdminUI clone = new AdminUI();
            clone.CopyFrom(entityToModify);

            if (this.editorService.EditAdmin(clone) == true)
            {
                entityToModify.CopyFrom(clone);
                this.ModifyAdminInDB(clone);
                this.messengerService.Send("ADMIN WAS SUCCESSFULLY MODIFIED!", "+LogicResult");
            }
            else
            {
                this.messengerService.Send("ADMIN MODIFICATION WAS CANCELLED!", "-LogicResult");
            }
        }

        private static AdminUI ConvertFromDatabase(Administrator admin)
        {
            AdminUI temp = new AdminUI()
            {
                Id = admin.ID,
                Name = admin.Name,
                Email = admin.Email,
                OfficeID = admin.OfficeID,
                Rank = admin.Rank,
                Salary = admin.Salary,
            };

            return temp;
        }

        private void ModifyAdminInDB(AdminUI admin)
        {
            if (admin != null)
            {
                this.logic.UpdateAdminName(admin.Id, admin.Name);
                this.logic.UpdateAdminEmail(admin.Id, admin.Email);
                this.logic.UpdateAdminRank(admin.Id, admin.Rank);
                this.logic.UpdateAdminOffice(admin.Id, admin.OfficeID);
                this.logic.UpdateAdminSalary(admin.Id, admin.Salary);
            }
        }

        private void AddToDatabase(AdminUI newAdmin)
        {
            this.logic.AddAdmin(new Administrator()
            {
                Name = newAdmin.Name,
                Email = newAdmin.Email,
                OfficeID = newAdmin.OfficeID,
                Rank = newAdmin.Rank,
                Salary = newAdmin.Salary,
            });
        }
    }
}
