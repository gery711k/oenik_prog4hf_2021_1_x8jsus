﻿// <copyright file="IUILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Generic logic interface.
    /// </summary>
    /// <typeparam name="T">Ui entity class.</typeparam>
    public interface IUILogic<T>
        where T : class
    {
        /// <summary>
        /// Adds an entity to the ui and the database.
        /// </summary>
        /// <param name="entityList">A list of entitie(ui).</param>
        void Add(IList<T> entityList);

        /// <summary>
        /// Modifies a selected entity(ui).
        /// </summary>
        /// <param name="entityToModify">The entity to modify.</param>
        void Modify(T entityToModify);

        /// <summary>
        /// Deletes an entity from the ui and the database.
        /// </summary>
        /// <param name="entityList">A list of entities(ui).</param>
        /// <param name="entityToDelete">The entity to delete.</param>
        void Delete(IList<T> entityList, T entityToDelete);

        /// <summary>
        /// Call all the T type entities from the database to the ui.
        /// </summary>
        /// <returns>Returns all the T type entities from the database to the ui.</returns>
        IList<T> GetAll();
    }
}
