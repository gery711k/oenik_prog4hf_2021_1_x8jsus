﻿// <copyright file="ToyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using ToyShop.Data;
    using ToyShop.Logic;
    using ToyShop.Wpf.Data;

    /// <summary>
    /// ToyLogic that modifies the ui and the database.
    /// </summary>
    public class ToyLogic : IUILogic<ToyUI>
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private IShopLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToyLogic"/> class.
        /// </summary>
        /// <param name="editorService">Editorservice interface.</param>
        /// <param name="messengerService">Messengerservice interface.</param>
        /// <param name="logic">Database logic.</param>
        public ToyLogic(IEditorService editorService, IMessenger messengerService, IShopLogic logic)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.logic = logic;
        }

        /// <inheritdoc/>
        public void Add(IList<ToyUI> entityList)
        {
            ToyUI newToy = new ToyUI();

            if (this.editorService.EditToy(newToy) == true)
            {
                entityList?.Add(newToy);
                this.AddToDatabase(newToy);
                entityList.Last().Id = this.logic.GetAllToys().Last().ID;
                this.messengerService.Send("NEW TOY WAS SUCCESFULLY ADDED!", "+LogicResult");
            }
            else
            {
                this.messengerService.Send("ADDING TOY WAS CANCELLED!", "-LogicResult");
            }
        }

        /// <inheritdoc/>
        public void Delete(IList<ToyUI> entityList, ToyUI entityToDelete)
        {
            if (entityToDelete != null && entityList != null && entityList.Remove(entityToDelete))
            {
                this.logic.DeleteToy(entityToDelete.Id);
                this.messengerService.Send("TOY WAS SUCCESSFULLY DELETED!", "+LogicResult");
            }
            else
            {
                this.messengerService.Send("YOU DON'T HAVE ANYTHING SELECTED!", "?LogicResult");
            }
        }

        /// <inheritdoc/>
        public IList<ToyUI> GetAll()
        {
            List<Toy> toys = this.logic.GetAllToys().ToList();
            List<ToyUI> list = new List<ToyUI>();

            foreach (var toy in toys)
            {
                list.Add(ConvertFromDatabase(toy));
            }

            return list;
        }

        /// <inheritdoc/>
        public void Modify(ToyUI entityToModify)
        {
            if (entityToModify == null)
            {
                this.messengerService.Send("YOU DON'T HAVE ANYTHING SELECTED!", "?LogicResult");
                return;
            }

            ToyUI clone = new ToyUI();
            clone.CopyFrom(entityToModify);

            if (this.editorService.EditToy(clone) == true)
            {
                entityToModify.CopyFrom(clone);
                this.ModifyToyInDB(clone);
                this.messengerService.Send("TOY WAS SUCCESSFULLY MODIFIED!", "+LogicResult");
            }
            else
            {
                this.messengerService.Send("TOY MODIFICATION WAS CANCELLED!", "-LogicResult");
            }
        }

        private static ToyUI ConvertFromDatabase(Toy toy)
        {
            ToyUI temp = new ToyUI()
            {
                Id = toy.ID,
                Name = toy.Name,
                Manufacturer = toy.Manufacturer,
                Category = toy.Category,
                Price = toy.Price,
                Quantity = toy.Quantity,
            };

            return temp;
        }

        private void ModifyToyInDB(ToyUI toy)
        {
            if (toy != null)
            {
                this.logic.UpdateToyName(toy.Id, toy.Name);
                this.logic.UpdateToyManufacturer(toy.Id, toy.Manufacturer);
                this.logic.UpdateToyPrice(toy.Id, toy.Price);
                this.logic.UpdateToyQuantity(toy.Id, toy.Quantity);
            }
        }

        private void AddToDatabase(ToyUI toy)
        {
            this.logic.AddToy(new Toy()
            {
                Name = toy.Name,
                Manufacturer = toy.Manufacturer,
                Category = toy.Category,
                Price = toy.Price,
                Quantity = toy.Quantity,
            });
        }
    }
}
