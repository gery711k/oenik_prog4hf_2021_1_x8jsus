﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ToyShop.Wpf.Data;

    /// <summary>
    /// Editor service interface.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Edit toy method.
        /// </summary>
        /// <param name="toy">ToyUI entity.</param>
        /// <returns>True if an edit was made to the toy, and false if there wasn't.</returns>
        bool EditToy(ToyUI toy);

        /// <summary>
        /// Edit admin method.
        /// </summary>
        /// <param name="admin">AdminUI entity.</param>
        /// <returns>True if an edit was made to the admin, and false if there wasn't.</returns>
        bool EditAdmin(AdminUI admin);

        /// <summary>
        /// Edit customer method.
        /// </summary>
        /// <param name="customer">CustomerUI entity.</param>
        /// <returns>True if an edit was made to the customer, and false if there wasn't.</returns>
        bool EditCustomer(CustomerUI customer);
    }
}
