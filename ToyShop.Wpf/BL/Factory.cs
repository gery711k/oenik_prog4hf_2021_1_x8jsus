﻿// <copyright file="Factory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.BL
{
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.EntityFrameworkCore;
    using ToyShop.Data;
    using ToyShop.Logic;
    using ToyShop.Repository;
    using ToyShop.Wpf.Data;
    using ToyShop.Wpf.UI;

    /// <summary>
    /// IoC register setup class.
    /// </summary>
    public static class Factory
    {
        /// <summary>
        /// Loads the database via IoC.
        /// </summary>
        public static void LoadDatabase()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);

            MyIoc.Instance.Register<DbContext, ToyShopContext>();
            MyIoc.Instance.Register<IToyRepository, ToyRepository>();
            MyIoc.Instance.Register<ICustomerRepository, CustomerRepository>();
            MyIoc.Instance.Register<IAdminRepository, AdminRepository>();
            MyIoc.Instance.Register<IOrderRepository, OrderRepository>();
            MyIoc.Instance.Register<IOrderedToyRepository, OrderedToyRepository>();
            MyIoc.Instance.Register<IEditorService, EditorService>();
            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIoc.Instance.Register<IUserLogic, UserLogic>();
            MyIoc.Instance.Register<IShopLogic, ShopLogic>();
            MyIoc.Instance.Register<IUILogic<ToyUI>, ToyLogic>();
            MyIoc.Instance.Register<IUILogic<AdminUI>, AdminLogic>();
            MyIoc.Instance.Register<IUILogic<CustomerUI>, CustomerLogic>();
        }
    }
}
