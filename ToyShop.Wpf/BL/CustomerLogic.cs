﻿// <copyright file="CustomerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using ToyShop.Data;
    using ToyShop.Logic;
    using ToyShop.Wpf.Data;

    /// <summary>
    /// Customer logic for ui.
    /// </summary>
    public class CustomerLogic : IUILogic<CustomerUI>
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private IUserLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerLogic"/> class.
        /// </summary>
        /// <param name="editorService">Editorservice interface.</param>
        /// <param name="messengerService">Messengerservice interface.</param>
        /// <param name="logic">Database logic class.</param>
        public CustomerLogic(IEditorService editorService, IMessenger messengerService, IUserLogic logic)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.logic = logic;
        }

        /// <inheritdoc/>
        public void Add(IList<CustomerUI> entityList)
        {
            CustomerUI newCustomer = new CustomerUI();

            if (this.editorService.EditCustomer(newCustomer) == true)
            {
                entityList?.Add(newCustomer);
                this.AddToDatabase(newCustomer);
                entityList.Last().Id = this.logic.GetAllCustomers().Last().ID;
                this.messengerService.Send("NEW CUSTOMER WAS SUCCESSFULLY ADDED!", "+LogicResult");
            }
            else
            {
                this.messengerService.Send("ADDING NEW CUSTOMER WAS CANCELLED!", "-LogicResult");
            }
        }

        /// <inheritdoc/>
        public void Delete(IList<CustomerUI> entityList, CustomerUI entityToDelete)
        {
            if (entityToDelete != null && entityList != null && entityList.Remove(entityToDelete))
            {
                this.logic.DeleteCustomer(entityToDelete.Id);
                this.messengerService.Send("CUSTOMER WAS SUCCESSFULLY DELETED!", "+LogicResult");
            }
            else
            {
                this.messengerService.Send("YOU DON'T HAVE ANYTHING SELECTED!", "?LogicResult");
            }
        }

        /// <inheritdoc/>
        public IList<CustomerUI> GetAll()
        {
            List<Customer> customers = this.logic.GetAllCustomers().ToList();
            List<CustomerUI> list = new List<CustomerUI>();

            foreach (var newCustomer in customers)
            {
                list.Add(ConvertFromDatabase(newCustomer));
            }

            return list;
        }

        /// <inheritdoc/>
        public void Modify(CustomerUI entityToModify)
        {
            if (entityToModify == null)
            {
                this.messengerService.Send("YOU DON'T HAVE ANYTHING SELECTED!", "?LogicResult");
                return;
            }

            CustomerUI clone = new CustomerUI();
            clone.CopyFrom(entityToModify);

            if (this.editorService.EditCustomer(clone) == true)
            {
                entityToModify.CopyFrom(clone);
                this.ModifyCustomerInDB(clone);
                this.messengerService.Send("CUSTOMER WAS SUCCESSFULLY MODIFIED!", "+LogicResult");
            }
            else
            {
                this.messengerService.Send("CUSTOMER MODIFICATION WAS CANCELLED!", "-LogicResult");
            }
        }

        private static CustomerUI ConvertFromDatabase(Customer customer)
        {
            CustomerUI temp = new CustomerUI()
            {
                Id = customer.ID,
                Name = customer.Name,
                Email = customer.Email,
                City = customer.City,
                Country = customer.Country,
                Password = customer.Password,
            };

            return temp;
        }

        private void ModifyCustomerInDB(CustomerUI cust)
        {
            if (cust != null)
            {
                this.logic.UpdateCustomerName(cust.Id, cust.Name);
                this.logic.UpdateCustomerEmail(cust.Id, cust.Email);
                this.logic.UpdateCustomerCity(cust.Id, cust.City);
                this.logic.UpdateCustomerCountry(cust.Id, cust.Country);
                this.logic.UpdateCustomerPassword(cust.Id, this.logic.GetCustomer(cust.Id).Password, cust.Password);
            }
        }

        private void AddToDatabase(CustomerUI newCustomer)
        {
            this.logic.AddCustomer(new Customer()
            {
                Name = newCustomer.Name,
                Email = newCustomer.Email,
                City = newCustomer.City,
                Country = newCustomer.Country,
                Password = newCustomer.Password,
            });
        }
    }
}
