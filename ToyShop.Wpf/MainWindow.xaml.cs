﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace ToyShop.Wpf
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using ToyShop.Wpf.VM;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        private ToyViewModel toyVM;
        private AdminViewModel adminVM;
        private CustomerViewModel customerVM;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.SetResources();
            this.RegisterResultMessages();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Unregister(this);
        }

        private void SetResources()
        {
            this.toyVM = this.FindResource("toyVM") as ToyViewModel;
            this.adminVM = this.FindResource("adminVM") as AdminViewModel;
            this.customerVM = this.FindResource("customerVM") as CustomerViewModel;
        }

        private void RegisterResultMessages()
        {
            Messenger.Default.Register<string>(this, "+LogicResult", msg => MessageBox.Show(msg, "SUCCESS!", MessageBoxButton.OK, MessageBoxImage.Information));
            Messenger.Default.Register<string>(this, "-LogicResult", msg => MessageBox.Show(msg, "ERROR!", MessageBoxButton.OK, MessageBoxImage.Error));
            Messenger.Default.Register<string>(this, "?LogicResult", msg => MessageBox.Show(msg, "WARNING!", MessageBoxButton.OK, MessageBoxImage.Warning));
        }
    }
}
