﻿// <copyright file="ToyEditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using ToyShop.Wpf.Data;
    using ToyShop.Wpf.VM;

    /// <summary>
    /// Interaction logic for EditorWindow.xaml.
    /// </summary>
    public partial class ToyEditorWindow : Window
    {
        private EditorViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToyEditorWindow"/> class.
        /// </summary>
        public ToyEditorWindow()
        {
            this.InitializeComponent();

            this.vm = this.FindResource("VM") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToyEditorWindow"/> class.
        /// </summary>
        /// <param name="toy">Editable toy.</param>
        public ToyEditorWindow(ToyUI toy)
            : this()
        {
            this.vm.Entity = toy;
        }

        /// <summary>
        /// Gets the editable toyui entity.
        /// </summary>
        public ToyUI Toy { get => this.vm.Entity as ToyUI; }

        private void OKClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
