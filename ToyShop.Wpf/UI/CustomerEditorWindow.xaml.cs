﻿// <copyright file="CustomerEditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using ToyShop.Wpf.Data;
    using ToyShop.Wpf.VM;

    /// <summary>
    /// Interaction logic for CustomerEditorWindow.xaml.
    /// </summary>
    public partial class CustomerEditorWindow : Window
    {
        private EditorViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerEditorWindow"/> class.
        /// </summary>
        public CustomerEditorWindow()
        {
            this.InitializeComponent();

            this.vm = this.FindResource("VM") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerEditorWindow"/> class.
        /// </summary>
        /// <param name="customer">Editable customer.</param>
        public CustomerEditorWindow(CustomerUI customer)
            : this()
        {
            this.vm.Entity = customer;
        }

        /// <summary>
        /// Gets the editable customerui entity.
        /// </summary>
        public CustomerUI Toy { get => this.vm.Entity as CustomerUI; }

        private void OKClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
