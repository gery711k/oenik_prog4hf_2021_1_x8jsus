﻿// <copyright file="AdminEditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using ToyShop.Wpf.Data;
    using ToyShop.Wpf.VM;

    /// <summary>
    /// Interaction logic for AdminEditorWindow.xaml.
    /// </summary>
    public partial class AdminEditorWindow : Window
    {
        private EditorViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminEditorWindow"/> class.
        /// </summary>
        public AdminEditorWindow()
        {
            this.InitializeComponent();

            this.vm = this.FindResource("VM") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminEditorWindow"/> class.
        /// </summary>
        /// <param name="admin">Editable admin.</param>
        public AdminEditorWindow(AdminUI admin)
            : this()
        {
            this.vm.Entity = admin;
        }

        /// <summary>
        /// Gets the editable adminui entity.
        /// </summary>
        public AdminUI Admin { get => this.vm.Entity as AdminUI; }

        private void OKClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
