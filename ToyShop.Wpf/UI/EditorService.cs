﻿// <copyright file="EditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Wpf.UI
{
    using ToyShop.Wpf.BL;
    using ToyShop.Wpf.Data;

    /// <summary>
    /// Editor service class.
    /// </summary>
    public class EditorService : IEditorService
    {
        /// <inheritdoc/>
        public bool EditAdmin(AdminUI admin)
        {
            AdminEditorWindow win = new AdminEditorWindow(admin);
            return win.ShowDialog() ?? false;
        }

        /// <inheritdoc/>
        public bool EditCustomer(CustomerUI customer)
        {
            CustomerEditorWindow win = new CustomerEditorWindow(customer);
            return win.ShowDialog() ?? false;
        }

        /// <inheritdoc/>
        public bool EditToy(ToyUI toy)
        {
            ToyEditorWindow win = new ToyEditorWindow(toy);
            return win.ShowDialog() ?? false;
        }
    }
}
