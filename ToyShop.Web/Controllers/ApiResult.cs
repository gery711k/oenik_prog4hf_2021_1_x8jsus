﻿// <copyright file="ApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Web.Controllers
{
    /// <summary>
    /// Single point of use class to get result of operation.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the operation was successful or not.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
