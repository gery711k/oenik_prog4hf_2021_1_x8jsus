﻿// <copyright file="ToysController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using ToyShop.Logic;
    using ToyShop.Web.Models;

    /// <summary>
    /// Controller class for actions on the Toys part of the web page.
    /// </summary>
    public class ToysController : Controller
    {
        private IShopLogic logic;
        private IMapper mapper;
        private ToyListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToysController"/> class.
        /// </summary>
        /// <param name="logic">Logic needed for db changes.</param>
        /// <param name="mapper">Mapper for coversions.</param>
        public ToysController(IShopLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new ToyListViewModel();
            this.vm.EditableToy = new Models.Toy();

            var toys = this.logic.GetAllToys();
            this.vm.ListOfToys = this.mapper.Map<IList<Data.Toy>, List<Models.Toy>>(toys);
        }

        /// <summary>
        /// The main index page.
        /// </summary>
        /// <returns>The index pages view.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("ToyIndex", this.vm);
        }

        // GET: Toys/Details/id

        /// <summary>
        /// The detail view page of an entity.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <returns>The detail view of an entity.</returns>
        public IActionResult Details(int id)
        {
            return this.View("ToyDetails", this.GetToyModel(id));
        }

        // GET: Toys/Remove/id

        /// <summary>
        /// Removes an entitity from the database, and the page.
        /// </summary>
        /// <param name="id">Id of the removable entity.</param>
        /// <returns>The new updated view.</returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete FAILED!";

            if (this.logic.DeleteToy(id))
            {
                this.TempData["editResult"] = "Delete SUCCEEDED!";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        // GET: Toys/Edit/id

        /// <summary>
        /// Edits the toy with the given id.
        /// </summary>
        /// <param name="id">Id of the editably toy.</param>
        /// <returns>The edited.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditableToy = this.GetToyModel(id);
            return this.View("ToyIndex", this.vm);
        }

        // POST: Toys/Edit/ + 1 toy + editAction

        /// <summary>
        /// Edits the toy with the given id.
        /// </summary>
        /// <param name="toy">The editable toy.</param>
        /// <param name="editAction">The edit action.</param>
        /// <returns>The refreshed index page.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Toy toy, string editAction)
        {
            if (this.ModelState.IsValid && toy != null)
            {
                this.TempData["editResult"] = "Edit SUCCEEDED!";
                if (editAction == "AddNew")
                {
                    this.logic.AddToy(new Data.Toy() { Name = toy.Name, Manufacturer = toy.Manufacturer, Category = toy.Category, Quantity = toy.Quantity, Price = toy.Price });
                }
                else
                {
                    Data.Toy someToy = this.logic.GetToy(toy.Id);
                    if (someToy.Name != toy.Name)
                    {
                        if (!this.logic.UpdateToyName(toy.Id, toy.Name))
                        {
                            this.TempData["editResult"] = "Edit FAILED!";
                        }
                    }

                    if (someToy.Manufacturer != toy.Manufacturer)
                    {
                        if (!this.logic.UpdateToyManufacturer(toy.Id, toy.Manufacturer))
                        {
                            this.TempData["editResult"] = "Edit FAILED!";
                        }
                    }

                    if (someToy.Price != toy.Price)
                    {
                        if (!this.logic.UpdateToyPrice(toy.Id, toy.Price))
                        {
                            this.TempData["editResult"] = "Edit FAILED!";
                        }
                    }

                    if (someToy.Quantity != toy.Quantity)
                    {
                        if (!this.logic.UpdateToyQuantity(toy.Id, toy.Quantity))
                        {
                            this.TempData["editResult"] = "Edit FAILED!";
                        }
                    }

                    if (someToy.Category != toy.Category)
                    {
                        if (!this.logic.UpdateToyCategory(toy.Id, toy.Category))
                        {
                            this.TempData["editResult"] = "Edit FAILED!";
                        }
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditableToy = toy;
                return this.View("ToyIndex", this.vm);
            }
        }

        private Models.Toy GetToyModel(int id)
        {
            Data.Toy oneToy = this.logic.GetToy(id);
            return this.mapper.Map<Data.Toy, Models.Toy>(oneToy);
        }
    }
}
