﻿// <copyright file="ToysApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using ToyShop.Logic;

    /// <summary>
    /// Toys API controller class.
    /// </summary>
    public partial class ToysApiController : Controller
    {
        private IShopLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToysApiController"/> class.
        /// </summary>
        /// <param name="logic">Shop logic.</param>
        /// <param name="mapper">Entity converter.</param>
        public ToysApiController(IShopLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets all of the Toy entities from the database.
        /// </summary>
        /// <returns>A collection of toys.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Toy> GetAll()
        {
            var toys = this.logic.GetAllToys();

            return this.mapper.Map<IList<Data.Toy>, List<Models.Toy>>(toys);
        }

        /// <summary>
        /// Deletes a toy from the database via API.
        /// </summary>
        /// <param name="id">ID of the deletable toy.</param>
        /// <returns>The result of the operation.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneToy(int id)
        {
            return new ApiResult() { OperationResult = this.logic.DeleteToy(id) };
        }

        /// <summary>
        /// Adds a new Toy to the database.
        /// </summary>
        /// <param name="toy">The new toy.</param>
        /// <returns>The result of the operation.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneToy(Models.Toy toy)
        {
            bool success = true;
            if (toy != null)
            {
                this.logic.AddToy(new Data.Toy() { Name = toy.Name, Manufacturer = toy.Manufacturer, Category = toy.Category, Quantity = toy.Quantity, Price = toy.Price });
            }
            else
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modifies a toy in the database via API.
        /// </summary>
        /// <param name="toy">The modifiable toy.</param>
        /// <returns>The result of the operation.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneToy(Models.Toy toy)
        {
            ApiResult res = new ApiResult();
            res.OperationResult = false;

            if (toy != null)
            {
                if (this.logic.UpdateToyName(toy.Id, toy.Name))
                {
                    res.OperationResult = true;
                }

                if (this.logic.UpdateToyManufacturer(toy.Id, toy.Manufacturer))
                {
                    res.OperationResult = true;
                }

                if (this.logic.UpdateToyCategory(toy.Id, toy.Category))
                {
                    res.OperationResult = true;
                }

                if (this.logic.UpdateToyQuantity(toy.Id, toy.Quantity))
                {
                    res.OperationResult = true;
                }

                if (this.logic.UpdateToyPrice(toy.Id, toy.Price))
                {
                    res.OperationResult = true;
                }
            }

            return res;
        }
    }
}
