﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<For gitstat true.", Scope = "member", Target = "~P:ToyShop.Web.Models.ToyListViewModel.ListOfToys")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<For gitstat true.>", Scope = "member", Target = "~P:ToyShop.Web.Models.ToyListViewModel.ListOfToys")]
