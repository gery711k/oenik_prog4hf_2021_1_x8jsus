﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// Converter between UI and Web entities.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Method for converting entities.
        /// </summary>
        /// <returns>A mapper that converts entites.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
           {
               cfg.CreateMap<Data.Toy, Models.Toy>().
                ForMember(dest => dest.Id, map =>
                    map.MapFrom(src => src.ID)).
                ForMember(dest => dest.Name, map =>
                    map.MapFrom(src => src.Name)).
                ForMember(dest => dest.Manufacturer, map =>
                    map.MapFrom(src => src.Manufacturer)).
                ForMember(dest => dest.Quantity, map =>
                    map.MapFrom(src => src.Quantity)).
                ForMember(dest => dest.Price, map =>
                    map.MapFrom(src => src.Price)).
                ForMember(dest => dest.Category, map =>
                    map.MapFrom(src => src.Category))
                .ReverseMap();
           });

            return config.CreateMapper();
        }
    }
}
