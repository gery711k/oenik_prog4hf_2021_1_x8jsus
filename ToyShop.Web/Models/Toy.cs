﻿// <copyright file="Toy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// A view model for the Data.Models.Toy.
    /// </summary>
    public class Toy
    {
        /// <summary>
        /// Gets or sets the id of Toy from Data.Models.
        /// </summary>
        [Display(Name = "ID")]
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of Toy from Data.Models.
        /// </summary>
        [Display(Name = "Name")]
        [Required]
        [StringLength(50, MinimumLength = 4)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer of Toy from Data.Models.
        /// </summary>
        [Display(Name = "Manufacturer")]
        [Required]
        [StringLength(50, MinimumLength = 4)]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the category of Toy from Data.Models.
        /// </summary>
        [Display(Name = "Category")]
        [Required]
        [StringLength(50, MinimumLength = 4)]
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the category of Toy from Data.Models.
        /// </summary>
        [Display(Name = "Quantity")]
        [Required]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the price of Toy from Data.Models.
        /// </summary>
        [Display(Name = "Price")]
        [Required]
        public int Price { get; set; }
    }
}
