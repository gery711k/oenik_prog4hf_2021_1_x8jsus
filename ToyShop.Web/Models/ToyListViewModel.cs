﻿// <copyright file="ToyListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Viewmodel for all of the toys from the database.
    /// </summary>
    public class ToyListViewModel
    {
        /// <summary>
        /// Gets or sets a list of toys from the database.
        /// </summary>
        public List<Toy> ListOfToys { get; set; }

        /// <summary>
        /// Gets or sets the selected toy to edit.
        /// </summary>
        public Toy EditableToy { get; set; }
    }
}
