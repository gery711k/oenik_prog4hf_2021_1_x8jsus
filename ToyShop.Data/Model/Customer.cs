﻿// <copyright file="Customer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This is the customers table.
    /// </summary>
    [Table("Customer")]
    public class Customer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        public Customer()
        {
            this.Orders = new HashSet<Order>();
        }

        /// <summary>
        /// Gets or sets the ID of a customer.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name of a customer.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the password of a customer.
        /// </summary>
        [Required]
        [MinLength(5)]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the country of a customer.
        /// </summary>
        [MaxLength(100)]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the city of a customer.
        /// </summary>
        [MaxLength(100)]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the email of a customer.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Gets the order collection. Navigation prop.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Order> Orders { get; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.ID,4} {this.Name,20} {this.Email,30}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            // Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Customer p = (Customer)obj;
                return this.ID == p.ID;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
