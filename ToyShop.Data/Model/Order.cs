﻿// <copyright file="Order.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This is the order table.
    /// </summary>
    [Table("Order")]
    public class Order
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Order"/> class.
        /// </summary>
        public Order()
        {
            this.OrderedToys = new HashSet<OrderedToy>();
        }

        /// <summary>
        /// Gets or sets the ID of the order.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the date of the order.
        /// </summary>
        [Required]
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// Gets or sets the ID of the order's administrator.
        /// </summary>
        [ForeignKey(nameof(Admin))]
        public int AdminID { get; set; }

        /// <summary>
        /// Gets or sets the admin of an order. Navigation property.
        /// </summary>
        [NotMapped]
        public virtual Administrator Admin { get; set; }

        /// <summary>
        /// Gets or sets the id of the order's customer.
        /// </summary>
        [ForeignKey(nameof(Customer))]
        public int CustomerID { get; set; }

        /// <summary>
        /// Gets or sets... Customer navigation property.
        /// </summary>
        [NotMapped]
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Gets the ordered toys collection. Navigation property.
        /// </summary>
        public virtual ICollection<OrderedToy> OrderedToys { get; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.ID,4} {this.AdminID,15} {this.CustomerID,15} {this.OrderDate,25}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            // Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Order p = (Order)obj;
                return this.ID == p.ID;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}