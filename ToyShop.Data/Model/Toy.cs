﻿// <copyright file="Toy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This is the toy table.
    /// </summary>
    [Table("Toy")]
    public class Toy
    {
        /// <summary>
        /// Gets or sets the ID of a toy.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name of a toy.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer of a toy.
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the category of a toy.
        /// </summary>
        [MaxLength(20)]
        [Required]
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the quantity of a toy.
        /// </summary>
        [Required]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the price of a toy.
        /// </summary>
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// Gets the OrderedToys collection. Navigation property.
        /// </summary>
        [NotMapped]
        public virtual ICollection<OrderedToy> OrderedToys { get; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.ID,4} {this.Manufacturer,20} {this.Name,20} {this.Price,8}Ft {this.Quantity,8} pieces left.";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            // Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()) || obj.GetType().Equals(typeof(OrderedToy)))
            {
                return false;
            }
            else
            {
                Toy p = (Toy)obj;
                return (this.Name == p.Name) && (this.Manufacturer == p.Manufacturer) && (this.Price == p.Price);
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
