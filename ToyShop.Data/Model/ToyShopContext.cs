﻿// <copyright file="ToyShopContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Data
{
    using System;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// This class contains the database essentials.
    /// </summary>
    public class ToyShopContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToyShopContext"/> class.
        /// </summary>
        public ToyShopContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets the table for toys.
        /// </summary>
        public virtual DbSet<Toy> Toys { get; set; }

        /// <summary>
        /// Gets or sets the table for customers.
        /// </summary>
        public virtual DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Gets or sets the table for employees.
        /// </summary>
        public virtual DbSet<Administrator> Admins { get; set; }

        /// <summary>
        /// Gets or sets the table for orders.
        /// </summary>
        public virtual DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Gets or sets the table for ordered toys.
        /// </summary>
        public virtual DbSet<OrderedToy> OrderedToys { get; set; }

        /// <summary>
        /// Provides a simple API surface for configuring DbContextOptionsBuilder.
        /// </summary>
        /// <param name="optionsBuilder"> Sets the connection for the database. </param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null)
            {
                if (!optionsBuilder.IsConfigured)
                {
                    optionsBuilder
                        .UseLazyLoadingProxies()
                        .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\ToyShopDatabase.mdf;Integrated Security=True");
                }
            }
        }

        /// <summary>
        /// Fills the database with content, and sets the relations between tables.
        /// </summary>
        /// <param name="modelBuilder"> Builds a entities. </param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Toys
            Toy t1 = new Toy
            {
                ID = 1,
                Name = "Lego House",
                Manufacturer = "Lego Factory",
                Category = "KIDS",
                Quantity = 100,
                Price = 1000,
            };
            Toy t2 = new Toy
            {
                ID = 2,
                Name = "Lego Falcon",
                Manufacturer = "Lego Factory",
                Category = "KIDS",
                Quantity = 100,
                Price = 1100,
            };
            Toy t3 = new Toy
            {
                ID = 3,
                Name = "Lego Vehicle",
                Manufacturer = "Lego Factory",
                Category = "KIDS",
                Quantity = 100,
                Price = 1200,
            };
            Toy t4 = new Toy
            {
                ID = 4,
                Name = "Football",
                Manufacturer = "UFC Facilities",
                Category = "SPORT",
                Quantity = 100,
                Price = 3000,
            };
            Toy t5 = new Toy
            {
                ID = 5,
                Name = "Basketball",
                Manufacturer = "NBA Facilities",
                Category = "SPORT",
                Quantity = 100,
                Price = 3000,
            };
            Toy t6 = new Toy
            {
                ID = 6,
                Name = "Jojo String",
                Manufacturer = "Agility",
                Category = "SPORT",
                Quantity = 100,
                Price = 1500,
            };
            Toy t7 = new Toy
            {
                ID = 7,
                Name = "Pen Spinner",
                Manufacturer = "Agility",
                Category = "SPORT",
                Quantity = 100,
                Price = 800,
            };
            Toy t8 = new Toy
            {
                ID = 8,
                Name = "Lambo",
                Manufacturer = "Carduplex",
                Category = "KIDS",
                Quantity = 100,
                Price = 2500,
            };
            Toy t9 = new Toy
            {
                ID = 9,
                Name = "Ferrari",
                Manufacturer = "Carduplex",
                Category = "KIDS",
                Quantity = 100,
                Price = 2800,
            };
            Toy t10 = new Toy
            {
                ID = 10,
                Name = "Dodge",
                Manufacturer = "Carduplex",
                Category = "KIDS",
                Quantity = 100,
                Price = 2200,
            };

            // Admins
            Administrator a1 = new Administrator
            {
                ID = 1,
                Name = "Kocsis Gergő",
                Rank = "Director",
                Email = "kocsis.gergo@email.hu",
                Salary = 5000000,
                OfficeID = 1,
            };
            Administrator a2 = new Administrator
            {
                ID = 2,
                Name = "Kósa Lajos",
                Rank = "Manager",
                Email = "kosa.lajos@email.hu",
                Salary = 2000000,
                OfficeID = 1,
            };
            Administrator a3 = new Administrator
            {
                ID = 3,
                Name = "Petőfi Sándor",
                Rank = "Seller",
                Email = "petofi.sandor@email.hu",
                Salary = 600000,
                OfficeID = 2,
            };
            Administrator a4 = new Administrator
            {
                ID = 4,
                Name = "Arany János",
                Rank = "Seller",
                Email = "arany.janos@email.hu",
                Salary = 600000,
                OfficeID = 2,
            };
            Administrator a5 = new Administrator
            {
                ID = 5,
                Name = "Kossuth Lajos",
                Rank = "Seller",
                Email = "kossuth.lajos@email.hu",
                Salary = 600000,
                OfficeID = 2,
            };

            // Customers
            Customer c1 = new Customer
            {
                Name = "Kovács Tímea",
                ID = 1,
                Password = "valami",
                Country = "Hungary",
                City = "Budapest",
                Email = "kovacs.timi@valami.hu",
            };
            Customer c2 = new Customer
            {
                Name = "Beka Kálmán",
                ID = 2,
                Password = "valami",
                Country = "Hungary",
                City = "Budapest",
                Email = "bekaka@valami.hu",
            };
            Customer c3 = new Customer
            {
                Name = "Vonnák Anna",
                ID = 3,
                Password = "valami",
                Country = "Hungary",
                City = "Budapest",
                Email = "v.anna@valami.hu",
            };
            Customer c4 = new Customer
            {
                Name = "Körm Ödön",
                ID = 4,
                Password = "valami",
                Country = "Hungary",
                City = "Érd",
                Email = "kormi@valami.hu",
            };

            // Orders
            Order o1 = new Order
            {
                ID = 1,
                AdminID = a3.ID,
                CustomerID = c1.ID,
                OrderDate = new DateTime(2020, 02, 10, 10, 22, 44),
            };
            Order o2 = new Order
            {
                ID = 2,
                AdminID = a3.ID,
                CustomerID = c1.ID,
                OrderDate = new DateTime(2020, 02, 10, 11, 33, 11),
            };
            Order o3 = new Order
            {
                ID = 3,
                AdminID = a3.ID,
                CustomerID = c1.ID,
                OrderDate = new DateTime(2020, 02, 19, 11, 33, 11),
            };
            Order o4 = new Order
            {
                ID = 4,
                AdminID = a3.ID,
                CustomerID = c2.ID,
                OrderDate = new DateTime(2020, 06, 12, 14, 25, 17),
            };
            Order o5 = new Order
            {
                ID = 5,
                AdminID = a4.ID,
                CustomerID = c2.ID,
                OrderDate = new DateTime(2020, 04, 30, 16, 22, 13),
            };
            Order o6 = new Order
            {
                ID = 6,
                AdminID = a4.ID,
                CustomerID = c3.ID,
                OrderDate = new DateTime(2020, 01, 17, 12, 22, 44),
            };
            Order o7 = new Order
            {
                ID = 7,
                AdminID = a4.ID,
                CustomerID = c3.ID,
                OrderDate = new DateTime(2020, 03, 11, 11, 11, 11),
            };
            Order o8 = new Order
            {
                ID = 8,
                AdminID = a4.ID,
                CustomerID = c3.ID,
                OrderDate = new DateTime(2020, 05, 05, 15, 55, 37),
            };
            Order o9 = new Order
            {
                ID = 9,
                AdminID = a5.ID,
                CustomerID = c4.ID,
                OrderDate = new DateTime(2020, 09, 30, 22, 26, 19),
            };
            Order o10 = new Order
            {
                ID = 10,
                AdminID = a5.ID,
                CustomerID = c4.ID,
                OrderDate = new DateTime(2020, 10, 05, 14, 25, 59),
            };

            // Ordered Toys
            OrderedToy ot1 = new OrderedToy
            {
                ID = 1,
                Price = t1.Price,
                Quantity = 1,
                OrderID = o1.ID,
                ToyID = t1.ID,
            };
            OrderedToy ot2 = new OrderedToy
            {
                ID = 2,
                Price = t2.Price,
                Quantity = 1,
                OrderID = o1.ID,
                ToyID = t2.ID,
            };
            OrderedToy ot3 = new OrderedToy
            {
                ID = 3,
                Price = t1.Price,
                Quantity = 1,
                OrderID = o1.ID,
                ToyID = t1.ID,
            };
            OrderedToy ot4 = new OrderedToy
            {
                ID = 4,
                Price = t1.Price,
                Quantity = 1,
                OrderID = o1.ID,
                ToyID = t1.ID,
            };
            OrderedToy ot5 = new OrderedToy
            {
                ID = 5,
                Price = t4.Price,
                Quantity = 2,
                OrderID = o1.ID,
                ToyID = t4.ID,
            };
            OrderedToy ot6 = new OrderedToy
            {
                ID = 6,
                Price = t5.Price,
                Quantity = 1,
                OrderID = o1.ID,
                ToyID = t5.ID,
            };
            OrderedToy ot7 = new OrderedToy
            {
                ID = 7,
                Price = t1.Price,
                Quantity = 1,
                OrderID = o1.ID,
                ToyID = t1.ID,
            };
            OrderedToy ot8 = new OrderedToy
            {
                ID = 8,
                Price = t7.Price,
                Quantity = 1,
                OrderID = o2.ID,
                ToyID = t7.ID,
            };
            OrderedToy ot9 = new OrderedToy
            {
                ID = 9,
                Price = t7.Price,
                Quantity = 1,
                OrderID = o2.ID,
                ToyID = t7.ID,
            };
            OrderedToy ot10 = new OrderedToy
            {
                ID = 10,
                Price = t8.Price,
                Quantity = 1,
                OrderID = o2.ID,
                ToyID = t8.ID,
            };

            // Setting up relations between tables.
            modelBuilder?.Entity<Customer>(entity =>
            {
                entity.HasMany(c => c.Orders)
                .WithOne(o => o.Customer)
                .HasForeignKey(c => c.CustomerID)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Administrator>(entity =>
            {
                entity.HasMany(a => a.Orders)
                .WithOne(o => o.Admin)
                .HasForeignKey(a => a.AdminID)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<OrderedToy>(entity =>
            {
                entity.HasOne(o => o.Order)
                .WithMany(b => b.OrderedToys)
                .HasForeignKey(o => o.OrderID)
                .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(ot => ot.Toy)
                .WithMany(t => t.OrderedToys)
                .HasForeignKey(ot => ot.ToyID)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasOne(o => o.Admin)
                .WithMany(a => a.Orders)
                .HasForeignKey(o => o.AdminID)
                .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(o => o.Customer)
                .WithMany(c => c.Orders)
                .HasForeignKey(o => o.CustomerID)
                .OnDelete(DeleteBehavior.Cascade);

                entity.HasMany(o => o.OrderedToys)
                .WithOne(ot => ot.Order)
                .HasForeignKey(o => o.OrderID)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Toy>(entity =>
            {
                entity.HasMany(t => t.OrderedToys)
                .WithOne(ot => ot.Toy)
                .HasForeignKey(ot => ot.ToyID)
                .OnDelete(DeleteBehavior.Cascade);
            });

            // Adding seed data to tabeles.
            modelBuilder.Entity<OrderedToy>()
                .HasData(ot1, ot2, ot3, ot4, ot5, ot6, ot7, ot8, ot9, ot10);

            modelBuilder.Entity<Toy>()
                .HasData(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);

            modelBuilder.Entity<Administrator>()
                .HasData(a1, a2, a3, a4, a5);

            modelBuilder.Entity<Customer>()
                .HasData(c1, c2, c3, c4);

            modelBuilder.Entity<Order>()
                .HasData(o1, o2, o3, o4, o5, o6, o7, o8, o9, o10);
        }
    }
}
