﻿// <copyright file="Administrator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace ToyShop.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This is the admins table.
    /// </summary>
    [Table("Administrator")]
    public class Administrator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Administrator"/> class.
        /// </summary>
        public Administrator()
        {
            this.Orders = new HashSet<Order>();
        }

        /// <summary>
        /// Gets or sets the ID of an admin.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name of an admin.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the rank of an admin.
        /// </summary>
        [Required]
        public string Rank { get; set; }

        /// <summary>
        /// Gets or sets the ID of the office the admin works in.
        /// </summary>
        [Required]
        public int OfficeID { get; set; }

        /// <summary>
        /// Gets or sets the email of an admin.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the salary of an administrator.
        /// </summary>
        [Required]
        public int Salary { get; set; }

        /// <summary>
        /// Gets the orders collection. Navigation prop.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Order> Orders { get; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.ID,4} {this.Name,20} {this.Rank,20} {this.OfficeID,10}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            // Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Administrator p = (Administrator)obj;
                return this.ID == p.ID;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
