﻿// <copyright file="OrderedToy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ToyShop.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// This is the ordered toy table.
    /// </summary>
    [Table("OrderedToy")]
    public class OrderedToy
    {
        /// <summary>
        /// Gets or sets the ID of a toy.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the quantity of a toy.
        /// </summary>
        [Required]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the price of a toy.
        /// </summary>
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets the id of an order.
        /// </summary>
        [ForeignKey(nameof(Order))]
        public int OrderID { get; set; }

        /// <summary>
        /// Gets or sets the id of a toy.
        /// </summary>
        [ForeignKey(nameof(Toy))]
        public int ToyID { get; set; }

        /// <summary>
        /// Gets or sets... order navigation property.
        /// </summary>
        [NotMapped]
        public virtual Order Order { get; set; }

        /// <summary>
        /// Gets or sets... toy navigation property.
        /// </summary>
        [NotMapped]
        public virtual Toy Toy { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.ID,4} {this.ToyID,8} {this.Quantity,10} {this.Price,9} {this.OrderID,10}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            // Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                return this.ID == (obj as OrderedToy).ID;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
