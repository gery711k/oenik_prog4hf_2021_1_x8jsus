var class_toy_shop_1_1_data_1_1_customer =
[
    [ "Customer", "class_toy_shop_1_1_data_1_1_customer.html#a0e14fbf9effc18b97a1b88300e5a346e", null ],
    [ "Equals", "class_toy_shop_1_1_data_1_1_customer.html#a57a99eab537c390ae3715898b1edbe8f", null ],
    [ "GetHashCode", "class_toy_shop_1_1_data_1_1_customer.html#a0c658ba8a9e0d9d95e7c7a1b2172ce74", null ],
    [ "ToString", "class_toy_shop_1_1_data_1_1_customer.html#a11242eedfa2a4a2d873a5a8dfa282ae4", null ],
    [ "City", "class_toy_shop_1_1_data_1_1_customer.html#a95432a6d65039e98c318c63ade280cc0", null ],
    [ "Country", "class_toy_shop_1_1_data_1_1_customer.html#ac389da17b95c6118c99ef5081e3f5899", null ],
    [ "Email", "class_toy_shop_1_1_data_1_1_customer.html#a3e09e8a6044e4a9206829ac475d4b83c", null ],
    [ "ID", "class_toy_shop_1_1_data_1_1_customer.html#a88a3a125e0f90441c65031c9b86fbc75", null ],
    [ "Name", "class_toy_shop_1_1_data_1_1_customer.html#acafecc412f1fcccd86fdef6d42496bee", null ],
    [ "Orders", "class_toy_shop_1_1_data_1_1_customer.html#a4802c6eb2f056470186c2a2edd7a7990", null ],
    [ "Password", "class_toy_shop_1_1_data_1_1_customer.html#aa993db98a3ddfd5473cfe7ec561e78fd", null ]
];