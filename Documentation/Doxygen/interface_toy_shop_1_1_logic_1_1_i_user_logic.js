var interface_toy_shop_1_1_logic_1_1_i_user_logic =
[
    [ "AddAdmin", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#aea398ec8e60681cecefff03fefc99aca", null ],
    [ "AddCustomer", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#afe822fa539073c67d44029acecc6bc67", null ],
    [ "DeleteAdmin", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#a3937c20abf23894850604941bb0ebc88", null ],
    [ "DeleteCustomer", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#af278f79e20ed4011eda0a69a30acc855", null ],
    [ "GetAdmin", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#ab6b7f910e14c1f6c97eb5455540a48d3", null ],
    [ "GetAllAdmins", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#a59fdc9b9aab57e289ecd23cb33c0885f", null ],
    [ "GetAllCustomers", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#a406906bc5715e4b86e25c0bb918bc478", null ],
    [ "GetCustomer", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#a67c9871cf156dd5b27d15181df3fbe8a", null ],
    [ "UpdateAdminEmail", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#a19cd0563fbeec808950a5437df0353b8", null ],
    [ "UpdateAdminName", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#a5f8b8a8ebffcfc0055e37694787c601d", null ],
    [ "UpdateAdminOffice", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#a56fce90cb1363fd4a503976428ada6fa", null ],
    [ "UpdateAdminRank", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#afcb281cf5371f451ef644f56d6f720de", null ],
    [ "UpdateAdminSalary", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#a92be0f9391b47d50f05f84b3ab66a9fa", null ],
    [ "UpdateCustomerCity", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#aebc246050396a42864115dbd9241821b", null ],
    [ "UpdateCustomerCountry", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#a574c672380ac013cf026d3d1b18066f7", null ],
    [ "UpdateCustomerEmail", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#afc7cd7edc01ccac5b7c928cfb4a7699a", null ],
    [ "UpdateCustomerName", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#a4f9065364a7da4896fabb21add9064cd", null ],
    [ "UpdateCustomerPassword", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html#affc08ccefdb63e306a3a75582d830d67", null ]
];