var searchData=
[
  ['officeid_377',['OfficeID',['../class_toy_shop_1_1_data_1_1_administrator.html#ab01a34a3407a9932c31d2a231f6b3071',1,'ToyShop::Data::Administrator']]],
  ['order_378',['Order',['../class_toy_shop_1_1_data_1_1_ordered_toy.html#ac8ab09183cc1c78052f69655e744de50',1,'ToyShop::Data::OrderedToy']]],
  ['orderdate_379',['OrderDate',['../class_toy_shop_1_1_data_1_1_order.html#a86e1510b92b8a3a07494f2a31f5713fa',1,'ToyShop::Data::Order']]],
  ['orderedtoys_380',['OrderedToys',['../class_toy_shop_1_1_data_1_1_order.html#a3c00a1d91a389a933abd58031bb70cb1',1,'ToyShop.Data.Order.OrderedToys()'],['../class_toy_shop_1_1_data_1_1_toy.html#a150fe6bbcd22d6e8dd0390c295fe4f6a',1,'ToyShop.Data.Toy.OrderedToys()'],['../class_toy_shop_1_1_data_1_1_toy_shop_context.html#a91bca799e68f0a951177a4971978f0e3',1,'ToyShop.Data.ToyShopContext.OrderedToys()']]],
  ['orderid_381',['OrderID',['../class_toy_shop_1_1_data_1_1_ordered_toy.html#a68a0a8ed136442ee4c928c59fc25aae3',1,'ToyShop::Data::OrderedToy']]],
  ['orders_382',['Orders',['../class_toy_shop_1_1_data_1_1_administrator.html#a5bcdea0786a1a25a11266054c41d6910',1,'ToyShop.Data.Administrator.Orders()'],['../class_toy_shop_1_1_data_1_1_customer.html#a4802c6eb2f056470186c2a2edd7a7990',1,'ToyShop.Data.Customer.Orders()'],['../class_toy_shop_1_1_data_1_1_toy_shop_context.html#a29b10888a29b41716d3e501fcf7457d4',1,'ToyShop.Data.ToyShopContext.Orders()']]]
];
