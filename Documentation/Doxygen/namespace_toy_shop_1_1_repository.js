var namespace_toy_shop_1_1_repository =
[
    [ "AdminRepository", "class_toy_shop_1_1_repository_1_1_admin_repository.html", "class_toy_shop_1_1_repository_1_1_admin_repository" ],
    [ "CustomerRepository", "class_toy_shop_1_1_repository_1_1_customer_repository.html", "class_toy_shop_1_1_repository_1_1_customer_repository" ],
    [ "GenericRepository", "class_toy_shop_1_1_repository_1_1_generic_repository.html", "class_toy_shop_1_1_repository_1_1_generic_repository" ],
    [ "IAdminRepository", "interface_toy_shop_1_1_repository_1_1_i_admin_repository.html", "interface_toy_shop_1_1_repository_1_1_i_admin_repository" ],
    [ "ICustomerRepository", "interface_toy_shop_1_1_repository_1_1_i_customer_repository.html", "interface_toy_shop_1_1_repository_1_1_i_customer_repository" ],
    [ "IOrderedToyRepository", "interface_toy_shop_1_1_repository_1_1_i_ordered_toy_repository.html", "interface_toy_shop_1_1_repository_1_1_i_ordered_toy_repository" ],
    [ "IOrderRepository", "interface_toy_shop_1_1_repository_1_1_i_order_repository.html", "interface_toy_shop_1_1_repository_1_1_i_order_repository" ],
    [ "IRepository", "interface_toy_shop_1_1_repository_1_1_i_repository.html", "interface_toy_shop_1_1_repository_1_1_i_repository" ],
    [ "IToyRepository", "interface_toy_shop_1_1_repository_1_1_i_toy_repository.html", "interface_toy_shop_1_1_repository_1_1_i_toy_repository" ],
    [ "OrderedToyRepository", "class_toy_shop_1_1_repository_1_1_ordered_toy_repository.html", "class_toy_shop_1_1_repository_1_1_ordered_toy_repository" ],
    [ "OrderRepository", "class_toy_shop_1_1_repository_1_1_order_repository.html", "class_toy_shop_1_1_repository_1_1_order_repository" ],
    [ "ToyRepository", "class_toy_shop_1_1_repository_1_1_toy_repository.html", "class_toy_shop_1_1_repository_1_1_toy_repository" ]
];