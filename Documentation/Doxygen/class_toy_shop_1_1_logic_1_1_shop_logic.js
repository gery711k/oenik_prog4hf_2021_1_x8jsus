var class_toy_shop_1_1_logic_1_1_shop_logic =
[
    [ "ShopLogic", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a0340bbe3eddc05f02c3cd2f6318430c7", null ],
    [ "AddOrder", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a59706ff4cca653be8584e30956d71f0d", null ],
    [ "AddToy", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a188f30e56703be411dc1a0f5c48317fc", null ],
    [ "AddToyToOrder", "class_toy_shop_1_1_logic_1_1_shop_logic.html#adf536de733f27a6b6b73553a98cb34b3", null ],
    [ "ChangeToyQuantityInOrder", "class_toy_shop_1_1_logic_1_1_shop_logic.html#aff42f3699a238fe5b1fc23db2b636fe8", null ],
    [ "DeleteOrder", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a61473782010e4047245cd2fd16b2c298", null ],
    [ "DeleteToy", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a6c90bddfe40832b13f70e8cb3a03bb64", null ],
    [ "GetAllCustomerIdsWithGivenToyInOrder", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a1755b22e86c1c8e8ab2fafc3c3cd227c", null ],
    [ "GetAllCustomersWithGivenToyInOrderAsync", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a4e5cf5fcfedf63425724f2dac6fb0afb", null ],
    [ "GetAllOrderedToys", "class_toy_shop_1_1_logic_1_1_shop_logic.html#abb348c6e96f465cfc6802265ed36c5f1", null ],
    [ "GetAllOrders", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a973e433a24a1eb4085fbd848fc0a7ff5", null ],
    [ "GetAllOrdersValues", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a486c53fbdb0e8e2252f9cc0f533af2be", null ],
    [ "GetAllOrdersValuesAsync", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a8844b1ff0e09232cb9dc1446102c6684", null ],
    [ "GetAllToys", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a4397248b84becb21ec5a03e6a7337f58", null ],
    [ "GetAverageNumberOfToysInAllOrders", "class_toy_shop_1_1_logic_1_1_shop_logic.html#afcb6c22d1923f2660665dd41d44110d2", null ],
    [ "GetOrder", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a9d012666553d44bd6e07d3926923275c", null ],
    [ "GetOrderContent", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a6c6a3adfa12387b03006e9697b5bb89c", null ],
    [ "GetOrderContentAsync", "class_toy_shop_1_1_logic_1_1_shop_logic.html#abfe97b9a375f2111c920da05f08bc280", null ],
    [ "GetOrderedToy", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a8461d8f01d8f46d348e4f2a36c6c46c1", null ],
    [ "GetOrderValue", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a756e07cbb0b350dc7a32de789c6cb052", null ],
    [ "GetOrderValueAsync", "class_toy_shop_1_1_logic_1_1_shop_logic.html#ad1bc9f6411d05b667a1c67652a446aa0", null ],
    [ "GetToy", "class_toy_shop_1_1_logic_1_1_shop_logic.html#ab5f922baa60cb30766031f59f2d11fba", null ],
    [ "RemoveToyFromOrder", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a4a66e8f8f2d1f9428d84877b7df2f796", null ],
    [ "UpdateOrdersAdmin", "class_toy_shop_1_1_logic_1_1_shop_logic.html#ad51bca28796c851a6ad6fc0aaa1065d7", null ],
    [ "UpdateToyManufacturer", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a14c167c9293ce1e7f8c74048d10c95fe", null ],
    [ "UpdateToyName", "class_toy_shop_1_1_logic_1_1_shop_logic.html#abe012faffa5e076115659c5ddf3e1f2e", null ],
    [ "UpdateToyPrice", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a4aa95f4dc355787c80d7fd356ddfb95d", null ],
    [ "UpdateToyQuantity", "class_toy_shop_1_1_logic_1_1_shop_logic.html#a45bf53dcb27417ce5accc3610fae3e14", null ]
];