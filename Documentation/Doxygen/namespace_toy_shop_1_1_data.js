var namespace_toy_shop_1_1_data =
[
    [ "Administrator", "class_toy_shop_1_1_data_1_1_administrator.html", "class_toy_shop_1_1_data_1_1_administrator" ],
    [ "ConsoleExtension", "class_toy_shop_1_1_data_1_1_console_extension.html", "class_toy_shop_1_1_data_1_1_console_extension" ],
    [ "Customer", "class_toy_shop_1_1_data_1_1_customer.html", "class_toy_shop_1_1_data_1_1_customer" ],
    [ "Order", "class_toy_shop_1_1_data_1_1_order.html", "class_toy_shop_1_1_data_1_1_order" ],
    [ "OrderedToy", "class_toy_shop_1_1_data_1_1_ordered_toy.html", "class_toy_shop_1_1_data_1_1_ordered_toy" ],
    [ "Toy", "class_toy_shop_1_1_data_1_1_toy.html", "class_toy_shop_1_1_data_1_1_toy" ],
    [ "ToyShopContext", "class_toy_shop_1_1_data_1_1_toy_shop_context.html", "class_toy_shop_1_1_data_1_1_toy_shop_context" ]
];