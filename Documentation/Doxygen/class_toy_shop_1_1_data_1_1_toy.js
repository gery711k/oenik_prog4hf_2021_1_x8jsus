var class_toy_shop_1_1_data_1_1_toy =
[
    [ "Equals", "class_toy_shop_1_1_data_1_1_toy.html#ae5d2764895b7459c726d94de36ae1b44", null ],
    [ "GetHashCode", "class_toy_shop_1_1_data_1_1_toy.html#a6e5dbf1db8899bc2fcdfea76652f94f5", null ],
    [ "ToString", "class_toy_shop_1_1_data_1_1_toy.html#ac81526a2f4f0241340dfdac0eaa3b3de", null ],
    [ "Category", "class_toy_shop_1_1_data_1_1_toy.html#a6ccd4591e8a61ab424877e645e2356a9", null ],
    [ "ID", "class_toy_shop_1_1_data_1_1_toy.html#a684a630cd7e6f496f8f0030f1e0021c1", null ],
    [ "Manufacturer", "class_toy_shop_1_1_data_1_1_toy.html#ab2e6877c933f31897ab5db7bf5a58eaf", null ],
    [ "Name", "class_toy_shop_1_1_data_1_1_toy.html#a8589d84c15ec2a77123da5610dc2e3a8", null ],
    [ "OrderedToys", "class_toy_shop_1_1_data_1_1_toy.html#a150fe6bbcd22d6e8dd0390c295fe4f6a", null ],
    [ "Price", "class_toy_shop_1_1_data_1_1_toy.html#a407707a4d0e3d5b9579abc5287864dbe", null ],
    [ "Quantity", "class_toy_shop_1_1_data_1_1_toy.html#a81d2bb4e113311fd81d4379f825d2c31", null ]
];