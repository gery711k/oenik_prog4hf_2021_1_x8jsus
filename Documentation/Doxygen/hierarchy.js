var hierarchy =
[
    [ "ToyShop.Data.Administrator", "class_toy_shop_1_1_data_1_1_administrator.html", null ],
    [ "ToyShop.Data.ConsoleExtension", "class_toy_shop_1_1_data_1_1_console_extension.html", null ],
    [ "ToyShop.Data.Customer", "class_toy_shop_1_1_data_1_1_customer.html", null ],
    [ "DbContext", null, [
      [ "ToyShop.Data.ToyShopContext", "class_toy_shop_1_1_data_1_1_toy_shop_context.html", null ]
    ] ],
    [ "ToyShop.Program.Factory", "class_toy_shop_1_1_program_1_1_factory.html", null ],
    [ "ToyShop.Repository.GenericRepository< Administrator >", "class_toy_shop_1_1_repository_1_1_generic_repository.html", [
      [ "ToyShop.Repository.AdminRepository", "class_toy_shop_1_1_repository_1_1_admin_repository.html", null ]
    ] ],
    [ "ToyShop.Repository.GenericRepository< Customer >", "class_toy_shop_1_1_repository_1_1_generic_repository.html", [
      [ "ToyShop.Repository.CustomerRepository", "class_toy_shop_1_1_repository_1_1_customer_repository.html", null ]
    ] ],
    [ "ToyShop.Repository.GenericRepository< Order >", "class_toy_shop_1_1_repository_1_1_generic_repository.html", [
      [ "ToyShop.Repository.OrderRepository", "class_toy_shop_1_1_repository_1_1_order_repository.html", null ]
    ] ],
    [ "ToyShop.Repository.GenericRepository< OrderedToy >", "class_toy_shop_1_1_repository_1_1_generic_repository.html", [
      [ "ToyShop.Repository.OrderedToyRepository", "class_toy_shop_1_1_repository_1_1_ordered_toy_repository.html", null ]
    ] ],
    [ "ToyShop.Repository.GenericRepository< Toy >", "class_toy_shop_1_1_repository_1_1_generic_repository.html", [
      [ "ToyShop.Repository.ToyRepository", "class_toy_shop_1_1_repository_1_1_toy_repository.html", null ]
    ] ],
    [ "ToyShop.Repository.IRepository< T >", "interface_toy_shop_1_1_repository_1_1_i_repository.html", [
      [ "ToyShop.Repository.GenericRepository< T >", "class_toy_shop_1_1_repository_1_1_generic_repository.html", null ]
    ] ],
    [ "ToyShop.Repository.IRepository< Administrator >", "interface_toy_shop_1_1_repository_1_1_i_repository.html", [
      [ "ToyShop.Repository.IAdminRepository", "interface_toy_shop_1_1_repository_1_1_i_admin_repository.html", [
        [ "ToyShop.Repository.AdminRepository", "class_toy_shop_1_1_repository_1_1_admin_repository.html", null ]
      ] ]
    ] ],
    [ "ToyShop.Repository.IRepository< Customer >", "interface_toy_shop_1_1_repository_1_1_i_repository.html", [
      [ "ToyShop.Repository.ICustomerRepository", "interface_toy_shop_1_1_repository_1_1_i_customer_repository.html", [
        [ "ToyShop.Repository.CustomerRepository", "class_toy_shop_1_1_repository_1_1_customer_repository.html", null ]
      ] ]
    ] ],
    [ "ToyShop.Repository.IRepository< Order >", "interface_toy_shop_1_1_repository_1_1_i_repository.html", [
      [ "ToyShop.Repository.IOrderRepository", "interface_toy_shop_1_1_repository_1_1_i_order_repository.html", [
        [ "ToyShop.Repository.OrderRepository", "class_toy_shop_1_1_repository_1_1_order_repository.html", null ]
      ] ]
    ] ],
    [ "ToyShop.Repository.IRepository< OrderedToy >", "interface_toy_shop_1_1_repository_1_1_i_repository.html", [
      [ "ToyShop.Repository.IOrderedToyRepository", "interface_toy_shop_1_1_repository_1_1_i_ordered_toy_repository.html", [
        [ "ToyShop.Repository.OrderedToyRepository", "class_toy_shop_1_1_repository_1_1_ordered_toy_repository.html", null ]
      ] ]
    ] ],
    [ "ToyShop.Repository.IRepository< Toy >", "interface_toy_shop_1_1_repository_1_1_i_repository.html", [
      [ "ToyShop.Repository.IToyRepository", "interface_toy_shop_1_1_repository_1_1_i_toy_repository.html", [
        [ "ToyShop.Repository.ToyRepository", "class_toy_shop_1_1_repository_1_1_toy_repository.html", null ]
      ] ]
    ] ],
    [ "ToyShop.Logic.IShopLogic", "interface_toy_shop_1_1_logic_1_1_i_shop_logic.html", [
      [ "ToyShop.Logic.ShopLogic", "class_toy_shop_1_1_logic_1_1_shop_logic.html", null ]
    ] ],
    [ "ToyShop.Logic.IUserLogic", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html", [
      [ "ToyShop.Logic.UserLogic", "class_toy_shop_1_1_logic_1_1_user_logic.html", null ]
    ] ],
    [ "ToyShop.Program.MenuUI", "class_toy_shop_1_1_program_1_1_menu_u_i.html", null ],
    [ "ToyShop.Data.Order", "class_toy_shop_1_1_data_1_1_order.html", null ],
    [ "ToyShop.Data.OrderedToy", "class_toy_shop_1_1_data_1_1_ordered_toy.html", null ],
    [ "ToyShop.Program.Program", "class_toy_shop_1_1_program_1_1_program.html", null ],
    [ "ToyShop.Logic.Tests.ShopLogicTests", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html", null ],
    [ "ToyShop.Data.Toy", "class_toy_shop_1_1_data_1_1_toy.html", null ],
    [ "ToyShop.Logic.Tests.UserLogicTests", "class_toy_shop_1_1_logic_1_1_tests_1_1_user_logic_tests.html", null ]
];