var class_toy_shop_1_1_repository_1_1_toy_repository =
[
    [ "ToyRepository", "class_toy_shop_1_1_repository_1_1_toy_repository.html#ab59f7d05027a15e1856a5e9c3b6085d2", null ],
    [ "GetOne", "class_toy_shop_1_1_repository_1_1_toy_repository.html#a5b053d7c82627d0f91e437ddfb1f25b2", null ],
    [ "UpdateManufacturer", "class_toy_shop_1_1_repository_1_1_toy_repository.html#a3800dc586150ed6962266685ab6e0075", null ],
    [ "UpdateName", "class_toy_shop_1_1_repository_1_1_toy_repository.html#a147bea5c7fe8e97177b7321e4de7d257", null ],
    [ "UpdatePrice", "class_toy_shop_1_1_repository_1_1_toy_repository.html#a7d07762a40f44190797083206636b92a", null ],
    [ "UpdateQuantity", "class_toy_shop_1_1_repository_1_1_toy_repository.html#a78d8e1c4c9ebe3e4d0b3e5544fcf3dc8", null ]
];