var class_toy_shop_1_1_data_1_1_administrator =
[
    [ "Administrator", "class_toy_shop_1_1_data_1_1_administrator.html#a318f9f7d2719f258322a046226097963", null ],
    [ "Equals", "class_toy_shop_1_1_data_1_1_administrator.html#a1fb9d9bb77698e7695b2d2fa09f67fb2", null ],
    [ "GetHashCode", "class_toy_shop_1_1_data_1_1_administrator.html#a2278f27b9fcb32cc505672ef99a70b27", null ],
    [ "ToString", "class_toy_shop_1_1_data_1_1_administrator.html#a91ca0969c1567365ff2cabe4faefe0f8", null ],
    [ "Email", "class_toy_shop_1_1_data_1_1_administrator.html#a9d6b425b6d8f8087257c3b7cf08b4e5f", null ],
    [ "ID", "class_toy_shop_1_1_data_1_1_administrator.html#a98382f91a740464b2fa66f26df4e5850", null ],
    [ "Name", "class_toy_shop_1_1_data_1_1_administrator.html#a5b3b3b02f8701ebcf65dbaf7276f36f2", null ],
    [ "OfficeID", "class_toy_shop_1_1_data_1_1_administrator.html#ab01a34a3407a9932c31d2a231f6b3071", null ],
    [ "Orders", "class_toy_shop_1_1_data_1_1_administrator.html#a5bcdea0786a1a25a11266054c41d6910", null ],
    [ "Rank", "class_toy_shop_1_1_data_1_1_administrator.html#aaad738b89c91138f3d679ec9e785ac1f", null ],
    [ "Salary", "class_toy_shop_1_1_data_1_1_administrator.html#afaa0a2b73dc38efb521cbdae4445c7d9", null ]
];