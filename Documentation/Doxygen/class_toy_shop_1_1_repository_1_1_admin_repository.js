var class_toy_shop_1_1_repository_1_1_admin_repository =
[
    [ "AdminRepository", "class_toy_shop_1_1_repository_1_1_admin_repository.html#a3e1a0bb683e495428c64d97a99928393", null ],
    [ "GetOne", "class_toy_shop_1_1_repository_1_1_admin_repository.html#a0bad2b76c36901c360ba8b645827404e", null ],
    [ "UpdateEmail", "class_toy_shop_1_1_repository_1_1_admin_repository.html#a6b7073293baa42016d8244a25f1b33ae", null ],
    [ "UpdateName", "class_toy_shop_1_1_repository_1_1_admin_repository.html#ae2d428911fcdd1e5ad64d0ca47b18862", null ],
    [ "UpdateOfficeID", "class_toy_shop_1_1_repository_1_1_admin_repository.html#a1b746b73878a71a78036b09c25ae6299", null ],
    [ "UpdateRank", "class_toy_shop_1_1_repository_1_1_admin_repository.html#a51575dd82c6fcefbbad55b584c5b81ad", null ],
    [ "UpdateSalary", "class_toy_shop_1_1_repository_1_1_admin_repository.html#ae25629000c6381996a035ca97c823057", null ]
];