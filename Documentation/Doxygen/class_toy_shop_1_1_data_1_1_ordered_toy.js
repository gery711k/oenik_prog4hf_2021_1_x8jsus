var class_toy_shop_1_1_data_1_1_ordered_toy =
[
    [ "Equals", "class_toy_shop_1_1_data_1_1_ordered_toy.html#a81bbf00840fe69d556de33eee0b7fcf4", null ],
    [ "GetHashCode", "class_toy_shop_1_1_data_1_1_ordered_toy.html#a3be2827a0c6de9dfe630616d768342ac", null ],
    [ "ToString", "class_toy_shop_1_1_data_1_1_ordered_toy.html#aa167402dc950d7743199d325aa829585", null ],
    [ "ID", "class_toy_shop_1_1_data_1_1_ordered_toy.html#a9fed8f28f999f43128073d986240e314", null ],
    [ "Order", "class_toy_shop_1_1_data_1_1_ordered_toy.html#ac8ab09183cc1c78052f69655e744de50", null ],
    [ "OrderID", "class_toy_shop_1_1_data_1_1_ordered_toy.html#a68a0a8ed136442ee4c928c59fc25aae3", null ],
    [ "Price", "class_toy_shop_1_1_data_1_1_ordered_toy.html#aa275326fa45d8efb8e6a8e75b0f14ba0", null ],
    [ "Quantity", "class_toy_shop_1_1_data_1_1_ordered_toy.html#a432a730e6108dd43f280eeab5a4b278b", null ],
    [ "Toy", "class_toy_shop_1_1_data_1_1_ordered_toy.html#a55d3414b1d1bb54e2076116a582ee6bb", null ],
    [ "ToyID", "class_toy_shop_1_1_data_1_1_ordered_toy.html#a7b1f44d612e05546a41035430c5845b4", null ]
];