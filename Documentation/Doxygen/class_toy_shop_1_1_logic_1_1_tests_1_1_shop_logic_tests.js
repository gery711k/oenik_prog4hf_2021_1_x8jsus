var class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests =
[
    [ "Setup", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#a286f86e1e0c7741a5e95955404212e97", null ],
    [ "TestAddNewOrder", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#abfb2cff52362c9ef507875602b2e149c", null ],
    [ "TestDeleteOrder", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#a10115e0409692b40f4db7cb28884c48a", null ],
    [ "TestDeleteOrderIdIsOutOfRange", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#a0ea0aaa19031136975d64a3e73a45f23", null ],
    [ "TestGetAllCustomersIdsWithGivenToyInOrder", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#a19b2a285aba6271673fada5584f5d6c2", null ],
    [ "TestGetAllOrdersValues", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#ae80575f094732e945513c46a943185b8", null ],
    [ "TestGetAllToys", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#af07c2661946263201000c9bcfcedd659", null ],
    [ "TestGetOneToy", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#a19ed1eebf310718edb50b624b3e34755", null ],
    [ "TestGetOneToyIdIsOutOfRange", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#a47932825aad4c5fb6ae57cba35a5a094", null ],
    [ "TestGetOrderContent", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#a226af4190071285bc3210ccd8f5e9734", null ],
    [ "TestGetOrderContentThrowsException", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#a127915954bf2c86f9c697c22f5c4b9e3", null ],
    [ "TestUpdateOrderedToyPrice", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#a7bf8926cca2c15444ef39c229aa2b73f", null ],
    [ "TestUpdateOrderedToyPriceIdIsOutOfRange", "class_toy_shop_1_1_logic_1_1_tests_1_1_shop_logic_tests.html#afa0da33e82abbb132bc7b3ca62fd2bfb", null ]
];