var namespace_toy_shop =
[
    [ "Data", "namespace_toy_shop_1_1_data.html", "namespace_toy_shop_1_1_data" ],
    [ "Logic", "namespace_toy_shop_1_1_logic.html", "namespace_toy_shop_1_1_logic" ],
    [ "Program", "namespace_toy_shop_1_1_program.html", "namespace_toy_shop_1_1_program" ],
    [ "Repository", "namespace_toy_shop_1_1_repository.html", "namespace_toy_shop_1_1_repository" ]
];