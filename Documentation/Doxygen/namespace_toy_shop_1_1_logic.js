var namespace_toy_shop_1_1_logic =
[
    [ "Tests", "namespace_toy_shop_1_1_logic_1_1_tests.html", "namespace_toy_shop_1_1_logic_1_1_tests" ],
    [ "IShopLogic", "interface_toy_shop_1_1_logic_1_1_i_shop_logic.html", "interface_toy_shop_1_1_logic_1_1_i_shop_logic" ],
    [ "IUserLogic", "interface_toy_shop_1_1_logic_1_1_i_user_logic.html", "interface_toy_shop_1_1_logic_1_1_i_user_logic" ],
    [ "ShopLogic", "class_toy_shop_1_1_logic_1_1_shop_logic.html", "class_toy_shop_1_1_logic_1_1_shop_logic" ],
    [ "UserLogic", "class_toy_shop_1_1_logic_1_1_user_logic.html", "class_toy_shop_1_1_logic_1_1_user_logic" ]
];