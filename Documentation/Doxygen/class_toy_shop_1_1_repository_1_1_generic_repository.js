var class_toy_shop_1_1_repository_1_1_generic_repository =
[
    [ "GenericRepository", "class_toy_shop_1_1_repository_1_1_generic_repository.html#a3da5e1b67a7fe2abc9a3c4f632b2d301", null ],
    [ "Add", "class_toy_shop_1_1_repository_1_1_generic_repository.html#a05bd10a288e04ee74bb153718b9cd753", null ],
    [ "Delete", "class_toy_shop_1_1_repository_1_1_generic_repository.html#ae9e4645ff53ddcb3c203f5fac1d55d8d", null ],
    [ "GetAll", "class_toy_shop_1_1_repository_1_1_generic_repository.html#a5b2bb70f7bbe3646a7578be9dd1cf426", null ],
    [ "GetOne", "class_toy_shop_1_1_repository_1_1_generic_repository.html#aa3821d5e24487efd43ca95e7ce81f473", null ],
    [ "Context", "class_toy_shop_1_1_repository_1_1_generic_repository.html#a03701bac666fcc0ac4df68576f7dc918", null ]
];