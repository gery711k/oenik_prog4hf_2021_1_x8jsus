var class_toy_shop_1_1_repository_1_1_customer_repository =
[
    [ "CustomerRepository", "class_toy_shop_1_1_repository_1_1_customer_repository.html#a9dd2e52df08cb6388d66667a844de965", null ],
    [ "GetOne", "class_toy_shop_1_1_repository_1_1_customer_repository.html#ac11f8b8b235608898871046b69d1a6a3", null ],
    [ "UpdateCity", "class_toy_shop_1_1_repository_1_1_customer_repository.html#ad394a9d8b0dc137f319124b862b4b95d", null ],
    [ "UpdateCountry", "class_toy_shop_1_1_repository_1_1_customer_repository.html#ae93790fca117af02f42e252dd6173f5c", null ],
    [ "UpdateEmail", "class_toy_shop_1_1_repository_1_1_customer_repository.html#a26e573811cc9dd0808ba22a6c0b6a6aa", null ],
    [ "UpdateName", "class_toy_shop_1_1_repository_1_1_customer_repository.html#a93d8acf0cc08abea770b0aefe08f6fc9", null ],
    [ "UpdatePassword", "class_toy_shop_1_1_repository_1_1_customer_repository.html#aebc3075e17ef96374ee1b7f3c621e472", null ]
];