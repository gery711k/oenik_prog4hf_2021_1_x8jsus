var class_toy_shop_1_1_data_1_1_console_extension =
[
    [ "ColoredUserInputString", "class_toy_shop_1_1_data_1_1_console_extension.html#a375a96d14da80455814c86ed5f49979e", null ],
    [ "ColoredWriter", "class_toy_shop_1_1_data_1_1_console_extension.html#a29904076a7cfa9a3218a341be314d3db", null ],
    [ "GetAdminHeader", "class_toy_shop_1_1_data_1_1_console_extension.html#a04b307e23756375ea574ffe3c29e84bf", null ],
    [ "GetCustomerHeader", "class_toy_shop_1_1_data_1_1_console_extension.html#af8b92af8d15b27fac42f11aa8b50fedf", null ],
    [ "GetNumberFromUser", "class_toy_shop_1_1_data_1_1_console_extension.html#aa416173ae3468201c2ac27e18c9bcc65", null ],
    [ "GetOrderedToyHeader", "class_toy_shop_1_1_data_1_1_console_extension.html#a43b6872fa2b24e3d4b5ba6c586d017b4", null ],
    [ "GetOrderHeader", "class_toy_shop_1_1_data_1_1_console_extension.html#a38298c848d680354e1ce5d54190d0440", null ],
    [ "GetToyHeader", "class_toy_shop_1_1_data_1_1_console_extension.html#ace6b1cccc7177f77b9fad4f371c9afc9", null ],
    [ "NormalWriter", "class_toy_shop_1_1_data_1_1_console_extension.html#a3a06ad097f5273c9efef740cac99aa01", null ]
];