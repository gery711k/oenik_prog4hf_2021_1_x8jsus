var class_toy_shop_1_1_data_1_1_order =
[
    [ "Order", "class_toy_shop_1_1_data_1_1_order.html#aaccf60c12d1691b8f1c350f3542fadc6", null ],
    [ "Equals", "class_toy_shop_1_1_data_1_1_order.html#a93bca99eff9110cbcd0953e6ce140fe1", null ],
    [ "GetHashCode", "class_toy_shop_1_1_data_1_1_order.html#a4ed5940587ea6ad53567d23ae6894731", null ],
    [ "ToString", "class_toy_shop_1_1_data_1_1_order.html#af9c4098cd749ddf26c2895fefd742248", null ],
    [ "Admin", "class_toy_shop_1_1_data_1_1_order.html#a3a9a539a0c55ff2dc13b469474ac0617", null ],
    [ "AdminID", "class_toy_shop_1_1_data_1_1_order.html#a27081995ab18cb75a0072d615885846a", null ],
    [ "Customer", "class_toy_shop_1_1_data_1_1_order.html#a2770d0a8d9809dd542cb82532e0131d0", null ],
    [ "CustomerID", "class_toy_shop_1_1_data_1_1_order.html#a84febeadda85dd841f7aff7357618fab", null ],
    [ "ID", "class_toy_shop_1_1_data_1_1_order.html#a1429e458d3c11c5777aed23d0d463bbf", null ],
    [ "OrderDate", "class_toy_shop_1_1_data_1_1_order.html#a86e1510b92b8a3a07494f2a31f5713fa", null ],
    [ "OrderedToys", "class_toy_shop_1_1_data_1_1_order.html#a3c00a1d91a389a933abd58031bb70cb1", null ]
];