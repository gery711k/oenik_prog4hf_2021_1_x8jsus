var class_toy_shop_1_1_repository_1_1_order_repository =
[
    [ "OrderRepository", "class_toy_shop_1_1_repository_1_1_order_repository.html#aa2a43c79f3060e182bc5c274a2997036", null ],
    [ "AddToyToOrder", "class_toy_shop_1_1_repository_1_1_order_repository.html#aa17a19d938a1c61e0c5d3350c460c797", null ],
    [ "GetOne", "class_toy_shop_1_1_repository_1_1_order_repository.html#aa8704953e3a9b26af187e5dfeba15d12", null ],
    [ "RemoveToyFromOrder", "class_toy_shop_1_1_repository_1_1_order_repository.html#a44f1a194733b9373bba827baca4a0fae", null ],
    [ "UpdateOrderAdmin", "class_toy_shop_1_1_repository_1_1_order_repository.html#ae631d69f316ce6bd7d8b2b14015a76a7", null ]
];