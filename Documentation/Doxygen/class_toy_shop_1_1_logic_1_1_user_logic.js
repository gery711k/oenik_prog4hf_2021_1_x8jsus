var class_toy_shop_1_1_logic_1_1_user_logic =
[
    [ "UserLogic", "class_toy_shop_1_1_logic_1_1_user_logic.html#acb60a71f0a0513da9b0ccdd6f8eb67b1", null ],
    [ "AddAdmin", "class_toy_shop_1_1_logic_1_1_user_logic.html#a95676ae60643867fd937028a4f252280", null ],
    [ "AddCustomer", "class_toy_shop_1_1_logic_1_1_user_logic.html#a51c8ae2bf56e0d07d281658d97e5bf55", null ],
    [ "DeleteAdmin", "class_toy_shop_1_1_logic_1_1_user_logic.html#a07788bedeb40ccab8885ea8c72e7120b", null ],
    [ "DeleteCustomer", "class_toy_shop_1_1_logic_1_1_user_logic.html#a1470ddd9198937cd9b0d4b2997c7dc3c", null ],
    [ "GetAdmin", "class_toy_shop_1_1_logic_1_1_user_logic.html#acf108d6600ac5c843f4b38282e5a45e6", null ],
    [ "GetAllAdmins", "class_toy_shop_1_1_logic_1_1_user_logic.html#a3afbae3e7e085cadbd32b220430c75c4", null ],
    [ "GetAllCustomers", "class_toy_shop_1_1_logic_1_1_user_logic.html#a06b75863730d3c733fa8c3e93228be93", null ],
    [ "GetCustomer", "class_toy_shop_1_1_logic_1_1_user_logic.html#a474dd3249a4f83939a6dcc29e6ca1d70", null ],
    [ "UpdateAdminEmail", "class_toy_shop_1_1_logic_1_1_user_logic.html#ad603bfd4f7eeffc8562743ed55b76c97", null ],
    [ "UpdateAdminName", "class_toy_shop_1_1_logic_1_1_user_logic.html#afda1fbb59800301519c0755093c06f5f", null ],
    [ "UpdateAdminOffice", "class_toy_shop_1_1_logic_1_1_user_logic.html#a2c4ece3a6b71f2d21ba944b4df26a3fc", null ],
    [ "UpdateAdminRank", "class_toy_shop_1_1_logic_1_1_user_logic.html#a153c4e521ee60bc9ffe4c6a1df83cb2a", null ],
    [ "UpdateAdminSalary", "class_toy_shop_1_1_logic_1_1_user_logic.html#a02f8b4d33bd5093321149034babc0cbf", null ],
    [ "UpdateCustomerCity", "class_toy_shop_1_1_logic_1_1_user_logic.html#a3d472f6610d492e5fd6aa096273444b9", null ],
    [ "UpdateCustomerCountry", "class_toy_shop_1_1_logic_1_1_user_logic.html#a437ce194636d96ce0ac405cb8e2dc156", null ],
    [ "UpdateCustomerEmail", "class_toy_shop_1_1_logic_1_1_user_logic.html#a166ab0d8770677e997aeafd23a607b7e", null ],
    [ "UpdateCustomerName", "class_toy_shop_1_1_logic_1_1_user_logic.html#a67f1197505caad7e8ebb33e9d6d3d6c2", null ],
    [ "UpdateCustomerPassword", "class_toy_shop_1_1_logic_1_1_user_logic.html#a99072f8a9fd16fdd5df62b97fbac08df", null ]
];