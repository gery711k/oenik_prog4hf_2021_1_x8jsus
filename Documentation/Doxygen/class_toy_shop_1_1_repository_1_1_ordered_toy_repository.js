var class_toy_shop_1_1_repository_1_1_ordered_toy_repository =
[
    [ "OrderedToyRepository", "class_toy_shop_1_1_repository_1_1_ordered_toy_repository.html#a18bce1dcda5eeb3f900add752dfc8cad", null ],
    [ "GetOne", "class_toy_shop_1_1_repository_1_1_ordered_toy_repository.html#aed5d63a942218f1b69266bb7c2e66dd9", null ],
    [ "UpdatePrice", "class_toy_shop_1_1_repository_1_1_ordered_toy_repository.html#af3933b3e7d661e8f9bf15f5175bad3d5", null ],
    [ "UpdateQuantity", "class_toy_shop_1_1_repository_1_1_ordered_toy_repository.html#ac6b5ed8fd59b6cd2c8fce39f65f64d48", null ]
];